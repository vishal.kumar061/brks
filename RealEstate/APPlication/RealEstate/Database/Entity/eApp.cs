namespace RealEstate.Database.Entity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class eApp : DbContext
    {
        public eApp()
            : base("name=eApp")
        {
        }

        public virtual DbSet<Favourite> Favourites { get; set; }
        public virtual DbSet<MA_ActionLog> MA_ActionLog { get; set; }
        public virtual DbSet<MA_Bank> MA_Bank { get; set; }
        public virtual DbSet<MA_ConfigFilter> MA_ConfigFilter { get; set; }
        public virtual DbSet<MA_Leave> MA_Leave { get; set; }
        public virtual DbSet<MA_Loan> MA_Loan { get; set; }
        public virtual DbSet<MA_Project> MA_Project { get; set; }
        public virtual DbSet<MA_Region> MA_Region { get; set; }
        public virtual DbSet<MA_Role> MA_Role { get; set; }
        public virtual DbSet<MA_User> MA_User { get; set; }
        public virtual DbSet<OTP> OTPs { get; set; }
        public virtual DbSet<Project_Images> Project_Images { get; set; }
        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<Property_Images> Property_Images { get; set; }
        public virtual DbSet<RoleUrlAccess> RoleUrlAccesses { get; set; }
        public virtual DbSet<UserAttendence> UserAttendences { get; set; }
        public virtual DbSet<UserBankDetail> UserBankDetails { get; set; }
        public virtual DbSet<UserInsentive> UserInsentives { get; set; }
        public virtual DbSet<UserLoan> UserLoans { get; set; }
        public virtual DbSet<UserPartialSalary> UserPartialSalaries { get; set; }
        public virtual DbSet<UserProject> UserProjects { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserSalary> UserSalaries { get; set; }
        public virtual DbSet<UserToken> UserTokens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MA_Bank>()
                .HasMany(e => e.UserBankDetails)
                .WithRequired(e => e.MA_Bank)
                .HasForeignKey(e => e.BankId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MA_Leave>()
                .HasMany(e => e.UserAttendences)
                .WithOptional(e => e.MA_Leave)
                .HasForeignKey(e => e.LeaveId);

            modelBuilder.Entity<MA_Loan>()
                .Property(e => e.Loan)
                .HasPrecision(9, 2);

            modelBuilder.Entity<MA_Loan>()
                .Property(e => e.Installement)
                .HasPrecision(9, 2);

            modelBuilder.Entity<MA_Loan>()
                .HasMany(e => e.UserLoans)
                .WithRequired(e => e.MA_Loan)
                .HasForeignKey(e => e.LoanId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MA_Project>()
                .HasMany(e => e.UserProjects)
                .WithOptional(e => e.MA_Project)
                .HasForeignKey(e => e.ProjectId);

            modelBuilder.Entity<MA_Role>()
                .HasMany(e => e.RoleUrlAccesses)
                .WithRequired(e => e.MA_Role)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MA_Role>()
                .HasMany(e => e.UserRoles)
                .WithOptional(e => e.MA_Role)
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.MA_Loan)
                .WithRequired(e => e.MA_User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserAttendences)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserBankDetails)
                .WithRequired(e => e.MA_User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserInsentives)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserPartialSalaries)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserProjects)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserRoles)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserSalaries)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserTokens)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserInsentive>()
                .Property(e => e.Amount)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserLoan>()
                .Property(e => e.Amount_Deducted)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserLoan>()
                .Property(e => e.Remaining_Amount)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserPartialSalary>()
                .Property(e => e.Amount)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Annual_Basic_Salary)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Annual_Special_Allowance)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Annual_PF_Amount)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.CTC)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Monthly_Fixed)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Loan)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.ESIC)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Calculated_Salary)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Paid)
                .HasPrecision(9, 2);

            modelBuilder.Entity<UserSalary>()
                .Property(e => e.Remaining_Amount)
                .HasPrecision(9, 2);
        }
    }
}
