namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MA_Loan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MA_Loan()
        {
            UserLoans = new HashSet<UserLoan>();
        }

        public int Id { get; set; }

        public int UserId { get; set; }

        public decimal Loan { get; set; }

        public decimal Installement { get; set; }

        public DateTime Date { get; set; }

        public bool IsActive { get; set; }

        [StringLength(500)]
        public string Mode { get; set; }

        [StringLength(500)]
        public string CF_subCode { get; set; }

        [StringLength(500)]
        public string CF_status { get; set; }

        [StringLength(500)]
        public string CF_message { get; set; }

        [StringLength(500)]
        public string TransferId { get; set; }

        [StringLength(500)]
        public string CF_referenceId { get; set; }

        [StringLength(500)]
        public string CF_utr { get; set; }

        public virtual MA_User MA_User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserLoan> UserLoans { get; set; }

        public bool IsDeleted { get; set; }
    }
}
