namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserSalary")]
    public partial class UserSalary
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        [StringLength(100)]
        public string Month_Year { get; set; }

        public decimal? Annual_Basic_Salary { get; set; }

        public decimal? Annual_Special_Allowance { get; set; }

        public decimal? Annual_PF_Amount { get; set; }

        public decimal? CTC { get; set; }

        public decimal? Monthly_Fixed { get; set; }

        public int? Total_Working_Days { get; set; }

        public int? LeaveAllocated { get; set; }

        public int? Paid_Leave { get; set; }

        public int? UnPaid_Leave { get; set; }

        public int? Total_Days_Worked { get; set; }

        public int? Remaining_Leave { get; set; }

        public decimal? Loan { get; set; }

        public decimal? ESIC { get; set; }

        public decimal? Calculated_Salary { get; set; }

        public decimal? Paid { get; set; }

        [StringLength(500)]
        public string Payment_Mode { get; set; }

        public decimal? Remaining_Amount { get; set; }

        [StringLength(1500)]
        public string Remarks { get; set; }

        [StringLength(500)]
        public string CF_referenceId { get; set; }

        [StringLength(500)]
        public string CF_utr { get; set; }

        [StringLength(500)]
        public string CF_subCode { get; set; }

        [StringLength(500)]
        public string CF_status { get; set; }

        [StringLength(500)]
        public string CF_message { get; set; }

        [StringLength(500)]
        public string TransferId { get; set; }

        public DateTime? Paid_On { get; set; }

        [StringLength(500)]
        public string Exception { get; set; }

        [StringLength(1)]
        public string IsDraft { get; set; }

        public virtual MA_User MA_User { get; set; }
    }
}
