namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserProject")]
    public partial class UserProject
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int? ProjectId { get; set; }

        public virtual MA_Project MA_Project { get; set; }

        public virtual MA_User MA_User { get; set; }
    }
}
