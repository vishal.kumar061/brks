namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MA_ConfigFilter
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string FilterType { get; set; }

        [StringLength(50)]
        public string FilterName { get; set; }

        [StringLength(50)]
        public string FilterValue { get; set; }

        public bool? IsActive { get; set; }
    }
}
