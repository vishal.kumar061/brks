namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MA_User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MA_User()
        {
            MA_Loan = new HashSet<MA_Loan>();
            UserAttendences = new HashSet<UserAttendence>();
            UserBankDetails = new HashSet<UserBankDetail>();
            UserInsentives = new HashSet<UserInsentive>();
            UserPartialSalaries = new HashSet<UserPartialSalary>();
            UserProjects = new HashSet<UserProject>();
            UserRoles = new HashSet<UserRole>();
            UserSalaries = new HashSet<UserSalary>();
            UserTokens = new HashSet<UserToken>();
        }

        public int Id { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Username { get; set; }

        [StringLength(500)]
        public string Password { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(100)]
        public string Designation { get; set; }

        [StringLength(500)]
        public string Email { get; set; }

        [StringLength(500)]
        public string Phone { get; set; }

        public decimal? Annual_Basic_Salary { get; set; }

        public decimal? Annual_Special_Allowance { get; set; }

        public decimal? Annual_PF_Amount { get; set; }

        public decimal? Annual_ESIC { get; set; }

        [StringLength(500)]
        public string PAN { get; set; }

        [StringLength(500)]
        public string Aadhar { get; set; }

        [StringLength(500)]
        public string UAN_No { get; set; }

        [StringLength(500)]
        public string PF_No { get; set; }

        [StringLength(500)]
        public string ProfileImage { get; set; }

        public int? LeaveAllocated { get; set; }

        [StringLength(500)]
        public string BeneId { get; set; }

        public int? Remaining_Leave { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MA_Loan> MA_Loan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserAttendence> UserAttendences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserBankDetail> UserBankDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserInsentive> UserInsentives { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserPartialSalary> UserPartialSalaries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserProject> UserProjects { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserRole> UserRoles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserSalary> UserSalaries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserToken> UserTokens { get; set; }
    }
}
