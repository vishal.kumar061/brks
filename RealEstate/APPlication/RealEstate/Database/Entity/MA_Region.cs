namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MA_Region
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string RegionName { get; set; }

        [StringLength(50)]
        public string RegionCode { get; set; }

        [StringLength(50)]
        public string CountryName { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        [StringLength(50)]
        public string StateName { get; set; }

        public bool? IsActive { get; set; }
    }
}
