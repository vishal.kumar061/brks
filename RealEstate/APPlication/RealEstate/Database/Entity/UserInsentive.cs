namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserInsentive")]
    public partial class UserInsentive
    {
        public int Id { get; set; }

        public int? UserId { get; set; }
        public string Mode { get; set; }

        public decimal? Amount { get; set; }

        public string Remarks { get; set; }

        public DateTime? Date { get; set; }

        public string CF_subCode { get; set; }
        public string CF_status { get; set; }
        public string CF_message { get; set; }
        public string TransferId { get; set; }
        public string CF_referenceId { get; set; }
        public string CF_utr { get; set; }

        public virtual MA_User MA_User { get; set; }
    }
}
