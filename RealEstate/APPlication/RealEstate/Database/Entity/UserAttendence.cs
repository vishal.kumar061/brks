namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserAttendence")]
    public partial class UserAttendence
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public bool? IsPresent { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(1500)]
        public string Location { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }

        public int? LeaveId { get; set; }

        public bool? IsApproved { get; set; }

        [StringLength(500)]
        public string Month_Year { get; set; }

        [StringLength(500)]
        public string Value { get; set; }

        public virtual MA_Leave MA_Leave { get; set; }

        public virtual MA_User MA_User { get; set; }
    }
}
