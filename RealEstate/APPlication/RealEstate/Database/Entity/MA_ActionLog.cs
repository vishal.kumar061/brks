namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MA_ActionLog
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string Url { get; set; }

        [StringLength(200)]
        public string IP { get; set; }

        [StringLength(200)]
        public string RequestType { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(200)]
        public string UserToken { get; set; }
    }
}
