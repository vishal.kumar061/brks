namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserPartialSalary")]
    public partial class UserPartialSalary
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        [StringLength(100)]
        public string Month_Year { get; set; }

        public decimal? Amount { get; set; }

        [StringLength(1500)]
        public string Remarks { get; set; }

        public DateTime? Paid_On { get; set; }

        [StringLength(500)]
        public string CF_referenceId { get; set; }

        [StringLength(500)]
        public string CF_utr { get; set; }

        [StringLength(500)]
        public string CF_subCode { get; set; }

        [StringLength(500)]
        public string CF_status { get; set; }

        [StringLength(500)]
        public string CF_message { get; set; }

        [StringLength(500)]
        public string TransferId { get; set; }

        public virtual MA_User MA_User { get; set; }
    }
}
