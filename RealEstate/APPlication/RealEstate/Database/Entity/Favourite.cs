namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Favourite")]
    public partial class Favourite
    {
        public int Id { get; set; }

        public int? PropertyId { get; set; }

        public int? UserId { get; set; }
    }
}
