namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Property")]
    public partial class Property
    {
        public int Id { get; set; }

        [StringLength(500)]
        public string Title { get; set; }

        public int? Project_Id { get; set; }

        public int? StateId { get; set; }

        [StringLength(3000)]
        public string Address { get; set; }

        public int? PinCode { get; set; }

        public double? Price { get; set; }

        public double? Cover_Area { get; set; }

        public double? Carpet_Area { get; set; }

        public int? BHK_Id_F { get; set; }

        public int? Status_Id_F { get; set; }

        public int? Firnishing_Id_F { get; set; }

        public int? Facing_Id_F { get; set; }

        public int? TotalFloor { get; set; }

        public int? CurrentFloor { get; set; }

        public double? MaintenanceCharge { get; set; }

        public double? BookingAmount { get; set; }

        public int? PropertyType_Id_F { get; set; }

        public bool? IsApproved { get; set; }

        public bool? IsActive { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        [StringLength(500)]
        public string Amenities { get; set; }

        public int? Property_Category_Id_F { get; set; }

        [StringLength(500)]
        public string Configuration { get; set; }

        [StringLength(500)]
        public string Society { get; set; }
    }
}
