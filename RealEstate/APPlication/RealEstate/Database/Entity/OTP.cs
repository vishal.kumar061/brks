namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OTP")]
    public partial class OTP
    {
        public int Id { get; set; }

        [StringLength(500)]
        public string Phone { get; set; }

        [Column("OTP")]
        [StringLength(500)]
        public string OTP1 { get; set; }

        public DateTime? CreatedOn { get; set; }

        public bool? IsActive { get; set; }
    }
}
