namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserBankDetail
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int BankId { get; set; }

        [StringLength(50)]
        public string Account_Number { get; set; }

        [StringLength(50)]
        public string IFSC { get; set; }

        [StringLength(100)]
        public string UPI { get; set; }

        public bool? Is_Active { get; set; }

        public virtual MA_Bank MA_Bank { get; set; }

        public virtual MA_User MA_User { get; set; }
    }
}
