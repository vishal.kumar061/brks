namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserToken")]
    public partial class UserToken
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        [StringLength(500)]
        public string Token { get; set; }

        public DateTime? IssuedOn { get; set; }

        public DateTime? ExpiresOn { get; set; }

        public bool? IsActive { get; set; }

        public virtual MA_User MA_User { get; set; }
    }
}
