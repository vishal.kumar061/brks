namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Project_Images
    {
        public int Id { get; set; }

        public int? ProjectId { get; set; }

        [StringLength(500)]
        public string ImageName { get; set; }

        public bool? IsActive { get; set; }
    }
}
