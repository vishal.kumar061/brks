namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RoleUrlAccess")]
    public partial class RoleUrlAccess
    {
        public int Id { get; set; }

        public int RoleId { get; set; }

        [Required]
        [StringLength(1000)]
        public string Access_Url { get; set; }

        public virtual MA_Role MA_Role { get; set; }
    }
}
