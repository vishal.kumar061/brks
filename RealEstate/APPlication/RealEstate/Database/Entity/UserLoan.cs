namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserLoan")]
    public partial class UserLoan
    {
        public int Id { get; set; }

        public int LoanId { get; set; }

        public decimal Amount_Deducted { get; set; }

        public decimal Remaining_Amount { get; set; }

        public DateTime Date { get; set; }

        public virtual MA_Loan MA_Loan { get; set; }
    }
}
