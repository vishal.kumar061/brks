﻿using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace RealEstate.Database.Service
{
    public interface IOTP_Service
    {
        DataResult<string> get_otp(OTP_GetModel model);
        DataResult<Responce_Auth_Model> validate_otp(OTP_ValidateModel model);
    }
    public class OTP_Service : IOTP_Service
    {
        private readonly eApp Context;
        private readonly IMA_User_Service MA_User_Service;

        public OTP_Service()
        {
            Context = new eApp();
            MA_User_Service = new MA_User_Service();
        }

        public void Add(OTP e)
        {
            Context.OTPs.Add(e);
            Context.SaveChanges();
        }
        public void Update(OTP e)
        {
            Context.Entry(e).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public DataResult<string> get_otp(OTP_GetModel model)
        {
            var result = new DataResult<string>();
            try
            {
                var user = MA_User_Service.GetByPhone(model.Mob);
                if (user != null)
                {
                    var rand = new Random();
                    var OTP = rand.Next().ToString().Replace(".", "").Substring(0, 6);

                    result = OTPHelper.Send_AWS_OTP("AKIAZC74SAHMFCAXHBDW", "t/TntZuQxWDHXUzZWnTAkHs4Ugi2nrkf5vNp/PBJ", "CALCI2018", "Your verification code is: " + OTP, model.Mob);
                    if (result.Success)
                    {
                        var entity = new OTP
                        {
                            CreatedOn = DateTime.Now,
                            IsActive = true,
                            OTP1 = OTP,
                            Phone = model.Mob
                        };
                        this.Add(entity);

                        result.Data = "OTP sent";
                        result.StatusCode = APIStatusCode.OK;
                        return result;
                    }
                    return result;
                }
                else
                {
                    result.Data = "Mobile number not registered";
                    result.StatusCode = APIStatusCode.EntityDoesNotExists;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = APIStatusCode.Oops;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                return result;
            }
        }

        public DataResult<Responce_Auth_Model> validate_otp(OTP_ValidateModel model)
        {

            var result = new DataResult<Responce_Auth_Model>();
            try
            {
                var data = (from x in Context.OTPs where x.Phone == model.Mob && x.IsActive == true && x.OTP1 == model.OTP orderby x.CreatedOn descending select x).FirstOrDefault();
                if (data != null)
                {
                    data.IsActive = false;
                    this.Update(data);
                    var user = MA_User_Service.GetByPhone(model.Mob);
                    result = MA_User_Service.AccessToken(new Req_Auth_Model { username = user.Username, pswd = user.Password });
                    return result;
                }
                else
                {
                    result.Errors.Add("Invalid Otp");
                    result.StatusCode = APIStatusCode.InvalidOtp;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = APIStatusCode.Oops;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                return result;
            }
        }
    }
}