﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RealEstate.Database.Service
{

    public interface IUserToken_Service
    {
        void Add(UserToken e);
        void Update(UserToken e);
        UserToken Get(int id);
        IList<UserToken> Get();
        bool ValidateToken(string tokenValue);
        UserToken Get_From_Session();
        UserToken Get(string token);
    }
    public class UserToken_Service : IUserToken_Service
    {
        private readonly eApp Context;
        public UserToken_Service()
        {
            Context = new eApp();
        }

        public void Add(UserToken e)
        {
            Context.UserTokens.Add(e);
            Context.SaveChanges();
        }
        public void Update(UserToken e)
        {
            Context.Entry(e).State = EntityState.Modified;
            Context.SaveChanges();
        }
        public UserToken Get(int id)
        {
            return Context.UserTokens.Find(id);
        }
        public UserToken Get(string token)
        {
            var flag = this.ValidateToken(token);
            if (flag)
                return Context.UserTokens.Where(x => x.Token == token).FirstOrDefault();
            else
                return null;
        }
        public IList<UserToken> Get()
        {
            return Context.UserTokens.ToList();
        }

        public void Delete(int Id)
        {
            var e = this.Get(Id);
            Context.UserTokens.Remove(e);
            Context.SaveChanges();
        }

        public bool ValidateToken(string tokenValue)
        {
            bool result = false;
            var data = (from x in Context.UserTokens
                        where x.Token.ToLower().Trim() == tokenValue.ToLower().Trim() &&
                         x.ExpiresOn > DateTime.Now
                        select x).ToList();
            if (data != null && data.Count > 0) result = true;

            return result;
        }

        public UserToken Get_From_Session()
        {
            var token = HttpContext.Current.Session["token"].ToString();
            return (from x in Context.UserTokens where x.Token == token select x).First();
        }
    }
}