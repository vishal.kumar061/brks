﻿using LinqToExcel;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Database.Service
{
    public interface IMA_User_Service
    {
        DataResult<Responce_Auth_Model> AccessToken(Req_Auth_Model model);
        IList<MA_User> Get();
        IList<MA_User> GetAll();
        MA_User Get(int id);
        MA_User GetByPhone(string Phone);
        IList<MA_Role> GetRole();
        DataResult<UserModel> AddEditUser(UserModel model);
        DataResult<UserModel> GetUser(int userid);
        DataResult<string> DeleteUser(int id);
        DataResult<string> SignUp(SignUpModel model);
        DataResult<string> ForgetPassword(ForgetPasswordModel model);

        //----------------------attendence
        DataResult<string> AddEdit_UserAttendence(UserAttendence model, bool IsAdmin);
        DataResult<string> Approve_Reject_Attendence(int userAttendenceId, bool isApproved);
        DataResult<AttendenceModel> GetAttendence(string month_year);
        DataResult<string> SaveAttendence(FormCollection fc);
        UserLeaveDetailsModel GetUserLeaveDetails(int UserId);
        User_Attendence_Excel_Model Upload(User_Attendence_Excel_Model model);
        void Save_Csv_Attendence();


        //----------------salary
        IList<UserSalaryModel_Base> GetSalary(string month_Year);
        IList<UserSalaryModel_Base> GenerateSalary(string Month_Year);
        DataResult<string> AJAX_Pay_Salary(IList<AJAX_Pay_Salary> model);
        DataResult<string> AJAX_View_Details(int UserId, string Month_year);
        DataResult<string> RefreshTransferStatus(int UserSalaryId);

        // --------------Insentive------------
        DataResult<InsentiveModel> AddEditInsentive(InsentiveModel model);
        IList<UserInsentive> GetAll_UserInsentive();
        InsentiveModel Get_UserInsentive(int Id);

    }
    public class MA_User_Service : IMA_User_Service
    {
        private readonly eApp Context;
        private readonly IEmailService EmailService;
        private readonly IMA_Leave_Service MA_Leave_Service;
        private readonly IMA_Loan_Service MA_Loan_Service;

        public MA_User_Service()
        {
            Context = new eApp();
            EmailService = new EmailService();
            MA_Leave_Service = new MA_Leave_Service();
            MA_Loan_Service = new MA_Loan_Service();
        }


        public IList<MA_User> Get()
        {
            return Context.MA_User.Where(x => x.IsActive == true).OrderBy(x => x.Name).ToList();
        }
        public IList<MA_User> GetAll()
        {
            return Context.MA_User.OrderBy(x => x.Name).ToList();
        }
        public MA_User Get(int id)
        {
            return Context.MA_User.Where(x => x.Id == id).FirstOrDefault();
        }
        public MA_User GetByPhone(string Phone)
        {
            return (from x in Context.MA_User where x.Phone == Phone && x.IsActive == true select x).FirstOrDefault();
        }
        public MA_User Get(string Email)
        {
            return (from x in Context.MA_User
                    where
                    x.Email.ToLower().Trim() == Email.ToLower().Trim()
                     && x.IsActive == true
                    select x).FirstOrDefault();
        }
        public IList<MA_User> GetAllByEmail(string Email)
        {
            return (from x in Context.MA_User
                    where
                    x.Email.ToLower().Trim() == Email.ToLower().Trim()
                     && x.IsActive == true
                    select x).ToList();
        }
        public IList<MA_Role> GetRole()
        {
            return Context.MA_Role.ToList();
        }


        public DataResult<string> SignUp(SignUpModel model)
        {
            var result = new DataResult<string>();
            try
            {
                var user = this.Get(model.Email);

                if (user == null)
                {
                    var rand = new Random();
                    var password = rand.Next().ToString().Replace(".", "").Substring(0, 5);

                    var role = new List<UserRole>();
                    role.Add(new UserRole { RoleId = 2 });
                    var entity = new MA_User
                    {
                        Name = model.Name,
                        Email = model.Email,
                        Username = model.Email,
                        IsActive = true,
                        Password = password,
                        Phone = model.Phone,
                        UserRoles = role
                    };
                    Context.MA_User.Add(entity);
                    Context.SaveChanges();
                    //send email with password

                    string Emailbody = string.Empty;
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Utility/Mailer/Welcome.html")))
                    {
                        Emailbody = reader.ReadToEnd();
                    }
                    Emailbody = Emailbody.Replace("{user}", model.Name);
                    Emailbody = Emailbody.Replace("{username}", model.Email);
                    Emailbody = Emailbody.Replace("{password}", password);

                    var msg = EmailService.email(new string[] { model.Email }, Emailbody, "BRKS: WELCOME !!");

                    if (msg.Success)
                        result.Data = "An email with password has been sent to your email.";
                    else
                        result.Data = "Your account is created but we could not send you mail due to some techinical error. Please contact administrator.";

                    result.StatusCode = APIStatusCode.Oops;
                    return result;
                }
                else
                {
                    result.Errors.Add("User with same email already exist");
                    result.StatusCode = APIStatusCode.DuplicateEntity;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = APIStatusCode.Oops;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                return result;
            }
        }
        public DataResult<string> ForgetPassword(ForgetPasswordModel model)
        {
            var result = new DataResult<string>();
            try
            {
                var user = this.Get(model.Email);
                if (user != null)
                {
                    //send email with password
                    string Emailbody = string.Empty;
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Utility/Mailer/Welcome.html")))
                    {
                        Emailbody = reader.ReadToEnd();
                    }
                    Emailbody = Emailbody.Replace("{user}", user.Name);
                    Emailbody = Emailbody.Replace("{username}", user.Username);
                    Emailbody = Emailbody.Replace("{password}", user.Password);

                    var msg = EmailService.email(new string[] { user.Email }, Emailbody, "BRKS: Account Password !!");

                    if (msg.Success)
                        result.Data = "An email with password has been sent to your registered email.";
                    else
                        result.Data = "Sorry we could not send you mail due to some techinical error. Please contact administrator.";

                    result.StatusCode = APIStatusCode.Oops;
                    return result;
                }
                else
                {
                    result.Errors.Add("Invalid Username");
                    result.StatusCode = APIStatusCode.DuplicateEntity;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = APIStatusCode.Oops;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                return result;
            }
        }
        public DataResult<Responce_Auth_Model> AccessToken(Req_Auth_Model model)
        {
            var result = new DataResult<Responce_Auth_Model>();

            var user = (from x in Context.MA_User
                        where
                        x.Username.ToLower().Trim() == model.username.ToLower().Trim() &&
                        x.Password.ToLower().Trim() == model.pswd.ToLower().Trim() &&
                        x.IsActive == true
                        select x).FirstOrDefault();

            if (user == null)
            {
                result.Errors.Add("Invalid username or password");
                result.StatusCode = APIStatusCode.Unauthorized;
                return result;
            }
            else
            {
                result.Data = new Responce_Auth_Model();
                result.Data.AccessToken = Guid.NewGuid().ToString();
                UserToken ut = new UserToken
                {
                    UserId = user.Id,
                    IsActive = true,
                    IssuedOn = DateTime.Now,
                    ExpiresOn = DateTime.Now.AddMinutes(20),
                    Token = result.Data.AccessToken
                };
                Context.UserTokens.Add(ut);
                Context.SaveChanges();

                result.Data.Id = user.Id;
                result.Data.UserName = user.Username;
                result.Data.IsAdmin = user.UserRoles.Any(x => x.RoleId == 1);
                result.Data.RoleName = string.Join(",", user.UserRoles.Select(x => x.MA_Role.Name));

                result.StatusCode = APIStatusCode.OK;
                return result;
            }
        }


        public DataResult<UserModel> AddEditUser(UserModel model)
        {
            var result = new DataResult<UserModel>();
            if (!model.IsAdmin)
            {
                result.Data = model;
                result.StatusCode = APIStatusCode.OK;
                return result;
            }

            try
            {
                if (model.ProfileImage != null)
                {
                    model.ProfileImage_Name = Regex.Replace(Guid.NewGuid().ToString().Substring(0, 7), @"[^0-9a-zA-Z]+", "") + "_" + Path.GetFileName(model.ProfileImage.FileName);
                    var ServerSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~//Utility//Uploads//ProfileImage//") + model.ProfileImage_Name);
                    model.ProfileImage.SaveAs(ServerSavePath);
                }

                if (model.Id == 0)
                    result.Data = this.AddUser(model);
                else if (model.Id != 0)
                    result.Data = this.EditUser(model);
            }
            catch (Exception ex)
            {
                result.message = ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
                return result;
            }
            result.message = string.IsNullOrEmpty(result.Data.AddBeneficiary.message) ? "SUCCESS" : result.Data.AddBeneficiary.message;
            result.StatusCode = APIStatusCode.OK;
            return result;
        }
        public UserModel AddUser(UserModel model)
        {

            var AddBeneficiary = new DataResult<string>();
            var result = new UserModel();

            var BeneId = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
            if (!string.IsNullOrEmpty(model.Account_Number) && !string.IsNullOrEmpty(model.IFSC))
            {
                var GetToken = Cashfree_Payouts_Config.GetToken();
                AddBeneficiary = Cashfree_Payouts_Config.AddBeneficiary(GetToken.Data, BeneId, model.Name, string.IsNullOrEmpty(model.Email) ? "brksbegs@gmail.com" : model.Email, string.IsNullOrEmpty(model.Mobile) ? "9999999999" : model.Mobile, model.Account_Number, model.IFSC, "Address Not Uploaded");
            }

            var entity = new MA_User
            {
                BeneId = BeneId,
                LeaveAllocated = model.UserLeaveDetail.Leave_Allocated,
                Remaining_Leave = model.UserLeaveDetail.Leave_Allocated,
                ProfileImage = model.ProfileImage_Name,
                Name = model.Name,
                Email = string.IsNullOrEmpty(model.Email) ? BeneId + "@gmail.com" : model.Email,
                Username = string.IsNullOrEmpty(model.Username) ? BeneId + "@gmail.com" : model.Username,
                Password = string.IsNullOrEmpty(model.Password) ? BeneId : model.Password,
                IsActive = true,
                Designation = model.Designation,

                Phone = model.Mobile,

                Annual_Basic_Salary = model.Monthly_Basic_Salary == null ? 0 : model.Monthly_Basic_Salary * 12,
                Annual_Special_Allowance = model.Monthly_Special_Allowance == null ? 0 : model.Monthly_Special_Allowance * 12,
                Annual_PF_Amount = model.Monthly_PF_Amount == null ? 0 : model.Monthly_PF_Amount * 12,
                Annual_ESIC = model.Monthly_ESIC == null ? 0 : model.Monthly_ESIC * 12,

                PAN = model.PAN,
                Aadhar = model.Aadhar,
                UAN_No = model.UAN_No,
                PF_No = model.PF_No,
                UserRoles = (from x in model.RoleId select new UserRole { RoleId = Convert.ToInt32(x) }).ToList(),
                UserProjects = model.ProjectId == null ? null : (from x in model.ProjectId select new UserProject { ProjectId = Convert.ToInt32(x) }).ToList(),
                UserBankDetails = !string.IsNullOrEmpty(model.Account_Number) ? (new List<UserBankDetail>
                        {
                            new UserBankDetail
                            {
                                BankId = Convert.ToInt32(model.BankId),
                                Account_Number = model.Account_Number,
                                IFSC = model.IFSC,
                                UPI = model.UPI,
                                Is_Active = AddBeneficiary.subCode == "200" ? true : false
                            }
                        }) : null,

            };
            model.AddBeneficiary = AddBeneficiary;
            Context.MA_User.Add(entity);
            Context.SaveChanges();

            model.Id = entity.Id;
            return model;
        }
        public UserModel EditUser(UserModel model)
        {

            var AddBeneficiary = new DataResult<string>();
            var result = new UserModel();
            var entity = this.Get(model.Id);
            entity.ProfileImage = model.ProfileImage_Name == "noImg.png" ? entity.ProfileImage : model.ProfileImage_Name;
            entity.Name = model.Name;
            entity.Username = model.Username;
            entity.Password = model.Password;
            entity.IsActive = true;
            entity.Designation = model.Designation;
            entity.Email = model.Email;
            entity.Phone = model.Mobile;
            entity.PAN = model.PAN;
            entity.Aadhar = model.Aadhar;
            entity.UAN_No = model.UAN_No;
            entity.PF_No = model.PF_No;

            if (model.IsAdmin)
            {
                entity.LeaveAllocated = model.UserLeaveDetail.Leave_Allocated;
                entity.Annual_Basic_Salary = model.Monthly_Basic_Salary == null ? 0 : model.Monthly_Basic_Salary * 12;
                entity.Annual_Special_Allowance = model.Monthly_Special_Allowance == null ? 0 : model.Monthly_Special_Allowance * 12;
                entity.Annual_PF_Amount = model.Monthly_PF_Amount == null ? 0 : model.Monthly_PF_Amount * 12;
                entity.Annual_ESIC = model.Monthly_ESIC == null ? 0 : model.Monthly_ESIC * 12;

                Context.UserRoles.RemoveRange(entity.UserRoles.ToList());
                Context.UserRoles.AddRange((from x in model.RoleId select new UserRole { UserId = entity.Id, RoleId = Convert.ToInt32(x) }));
                Context.UserProjects.RemoveRange(entity.UserProjects.ToList());
                if (model.ProjectId != null)
                {
                    Context.UserProjects.AddRange((from x in model.ProjectId select new UserProject { UserId = entity.Id, ProjectId = Convert.ToInt32(x) }));
                }
            }

            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();

            var GetToken = Cashfree_Payouts_Config.GetToken();
            var RemoveBeneficiary = Cashfree_Payouts_Config.RemoveBeneficiary(GetToken.Data, entity.BeneId);
            Context.UserBankDetails.RemoveRange(entity.UserBankDetails.ToList());
            Context.SaveChanges();

            if (model.BankId != null && !string.IsNullOrEmpty(model.Account_Number) && !string.IsNullOrEmpty(model.IFSC))
            {
                AddBeneficiary = Cashfree_Payouts_Config.AddBeneficiary(GetToken.Data, entity.BeneId, entity.Name, string.IsNullOrEmpty(entity.Email) ? "brksbegs@gmail.com" : entity.Email, string.IsNullOrEmpty(entity.Phone) ? "9999999999" : entity.Phone, model.Account_Number, model.IFSC, "Address Not Uploaded");

                Context.UserBankDetails.AddRange(new List<UserBankDetail>
                        {
                            new UserBankDetail
                            {
                                UserId = entity.Id,
                                BankId = Convert.ToInt32(model.BankId),
                                Account_Number = model.Account_Number,
                                IFSC = model.IFSC,
                                UPI = model.UPI,
                                Is_Active = AddBeneficiary.subCode == "200" ? true : false
                            }
                        });

                Context.Entry(entity).State = EntityState.Modified;
                Context.SaveChanges();
            }


            model.AddBeneficiary = AddBeneficiary;

            return model;
        }
        public DataResult<UserModel> GetUser(int userid)
        {
            var result = new DataResult<UserModel>();
            try
            {
                var user = this.Get(userid);

                result.Data = new UserModel
                {
                    User = user,
                    ProfileImage_Name = String.IsNullOrEmpty(user.ProfileImage) ? "noImg.png" : user.ProfileImage,
                    Id = user.Id,
                    Name = user.Name,
                    Username = user.Username,
                    Designation = user.Designation,
                    Email = user.Email,
                    Password = user.Password,
                    Mobile = user.Phone,

                    Monthly_Basic_Salary = user.Annual_Basic_Salary <= 0 ? 0 : user.Annual_Basic_Salary / 12,
                    Monthly_Special_Allowance = user.Annual_Special_Allowance <= 0 ? 0 : user.Annual_Special_Allowance / 12,
                    Monthly_PF_Amount = user.Annual_PF_Amount <= 0 ? 0 : user.Annual_PF_Amount / 12,
                    Monthly_ESIC = user.Annual_ESIC <= 0 ? 0 : user.Annual_ESIC / 12,

                    PAN = user.PAN,
                    Aadhar = user.Aadhar,
                    UAN_No = user.UAN_No,
                    PF_No = user.PF_No,
                    RoleId = user.UserRoles.Select(x => x.MA_Role.Id.ToString()).ToList(),
                    ProjectId = user.UserProjects.Select(x => x.MA_Project.Id.ToString()).ToList(),

                    BankId = user.UserBankDetails.Count() == 0 ? 0 : user.UserBankDetails.FirstOrDefault().BankId,
                    Account_Number = user.UserBankDetails.Count() == 0 ? "" : user.UserBankDetails.FirstOrDefault().Account_Number,
                    IFSC = user.UserBankDetails.Count() == 0 ? "" : user.UserBankDetails.FirstOrDefault().IFSC,
                    UPI = user.UserBankDetails.Count() == 0 ? "" : user.UserBankDetails.FirstOrDefault().UPI,
                    UserLeaveDetail = this.GetUserLeaveDetails(user.Id)
                };

            }
            catch (Exception ex)
            {
                result.message = ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
                return result;
            }
            result.message = "SUCCESS";
            result.StatusCode = APIStatusCode.OK;
            return result;
        }
        public DataResult<string> DeleteUser(int id)
        {
            var result = new DataResult<string>();
            try
            {
                var entity = this.Get(id);
                //Context.Properties.RemoveRange(entity.Properties);
                //Context.UserRoles.RemoveRange(entity.UserRoles);
                //Context.UserTokens.RemoveRange(entity.UserTokens);
                //Context.UserBankDetails.RemoveRange(entity.UserBankDetails);
                //Context.MA_User.Remove(entity);
                entity.IsActive = false;
                Context.Entry(entity).State = EntityState.Modified;
                Context.SaveChanges();

                result.StatusCode = APIStatusCode.OK;
                result.Data = "SUCCESS";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }



        //-----------------------Attendence---------------
        public DataResult<string> AddEdit_UserAttendence(UserAttendence model, bool IsAdmin)
        {
            var result = new DataResult<string>();
            try
            {

                var data = (from x in Context.UserAttendences
                            where x.UserId == model.UserId
                            select x).ToList().Where(x => Convert.ToDateTime(x.Date).Date == Convert.ToDateTime(model.Date).Date).ToList();
                if (data == null || data.Count == 0)
                {
                    Context.UserAttendences.Add(model);
                    Context.SaveChanges();
                    result.StatusCode = APIStatusCode.OK;
                    result.Data = "SUCCESS";
                }
                else if (IsAdmin == true)
                {
                    foreach (var x in data)
                    {
                        x.IsApproved = model.IsPresent == true ? (bool?)null : true;
                        x.Remarks = string.IsNullOrEmpty(model.Remarks) ? x.Remarks : model.Remarks;
                        x.IsPresent = model.IsPresent;
                        x.Location = model.Location;
                        x.LeaveId = model.IsPresent == true ? null : model.LeaveId == null ? x.LeaveId : model.LeaveId;
                        Context.Entry(x).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                }
                else if (!IsAdmin)
                {
                    foreach (var x in data)
                    {
                        x.Remarks = null;
                        x.IsApproved = null;
                        x.IsPresent = model.IsPresent;
                        x.Location = model.Location;
                        x.LeaveId = model.IsPresent == true ? null : model.LeaveId;
                        Context.Entry(x).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                }

                //-----------Set Remaining Leave------------
                var GetUserLeaveDetails = this.GetUserLeaveDetails((int)model.UserId);
                var user = this.Get((int)model.UserId);
                int? Remaining_Leave = (GetUserLeaveDetails.Leave_Allocated + GetUserLeaveDetails.Worked_On_Holiday) - GetUserLeaveDetails.Leave_Taken;
                user.Remaining_Leave = Remaining_Leave == null ? 0 : Remaining_Leave;
                Context.Entry(user).State = EntityState.Modified;
                Context.SaveChanges();

                result.StatusCode = APIStatusCode.OK;
                result.Data = "SUCCESS";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<string> Approve_Reject_Attendence(int userAttendenceId, bool isApproved)
        {
            var result = new DataResult<string>();
            try
            {
                var data = Context.UserAttendences.Find(userAttendenceId);
                data.IsApproved = isApproved;
                data.Remarks = isApproved ? "Leave Approved" : "Leave Rejected";
                Context.Entry(data).State = EntityState.Modified;
                Context.SaveChanges();
                result.StatusCode = APIStatusCode.OK;
                result.Data = "SUCCESS";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public UserLeaveDetailsModel GetUserLeaveDetails(int UserId)
        {
            if (UserId == 0)
                return new UserLeaveDetailsModel();

            var User = this.Get((int)UserId);
            var PublicHolidayList = MA_Leave_Service.GetAll().Where(x => x.IsPaid == false).ToList();
            var UserAttendence = (from x in Context.UserAttendences where x.UserId == UserId select x).ToList();

            var UserOnPublicHolidays = (from x in UserAttendence join y in PublicHolidayList on Convert.ToDateTime(x.Date).Date equals Convert.ToDateTime(y.FromDate).Date select x).ToList();

            var UserPresentOnPublicHoliday = UserOnPublicHolidays.Where(x => x.Value.ToUpper() == "P").ToList();

            var LeaveTaken = (from x in UserAttendence
                              where x.Value == "L" && x.IsApproved == true
                              select x).ToList().Except(UserOnPublicHolidays).ToList();


            var LeaveAllocated = User.LeaveAllocated == null ? 0 : User.LeaveAllocated;

            var model = new UserLeaveDetailsModel
            {
                Leave_Allocated = LeaveAllocated,
                Worked_On_Holiday = UserPresentOnPublicHoliday.Count,
                Leave_Taken = LeaveTaken.Count,
                Remaining_Leave = (LeaveAllocated + UserPresentOnPublicHoliday.Count) - LeaveTaken.Count
            };

            User.Remaining_Leave = (LeaveAllocated + UserPresentOnPublicHoliday.Count) - LeaveTaken.Count;
            Context.Entry(User).State = EntityState.Modified;
            Context.SaveChanges();

            return model;
        }
        public DataResult<AttendenceModel> GetAttendence(string month_year)
        {
            var result = new DataResult<AttendenceModel>();
            try
            {
                if (string.IsNullOrEmpty(month_year))
                {
                    CultureInfo ci = new CultureInfo("en-US");
                    month_year = DateTime.Now.ToString("MMM yyyy", ci);
                }

                var month_year_ = DateTime.ParseExact(month_year, "MMM yyyy", CultureInfo.CurrentCulture);
                int Total_Working_Days = DateTime.DaysInMonth(month_year_.Year, month_year_.Month);

                var users = this.Get();

                foreach (var usr in users)
                {
                    var attendence = usr.UserAttendences.ToList().Where(x => x.Date.Value.ToString("MMM yyyy") == month_year).ToList().OrderBy(z => z.Date).ToList();
                    usr.UserAttendences = attendence;
                }

                result.Data = new AttendenceModel
                {
                    Users = users,
                    total_days = Total_Working_Days,
                    total_employee = users.Count,
                    month_year = month_year,
                    leave_request = 0,
                    present = 0,
                    absent = 0
                };
                result.message = "SUCCESS";
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
            }
            return result;
        }
        public DataResult<string> SaveAttendence(FormCollection fc)
        {
            var result = new DataResult<string>();
            try
            {
                var leave = MA_Leave_Service.GetAll();
                var month_year = fc["month_year"].ToString();
                var month_year_ = DateTime.ParseExact(month_year, "MMM yyyy", CultureInfo.CurrentCulture);


                var UserAttendences = (from x in Context.UserAttendences where x.Month_Year == month_year select x).ToList();
                Context.UserAttendences.RemoveRange(UserAttendences);
                Context.SaveChanges();

                var list = new List<UserAttendence>();
                foreach (var key in fc.AllKeys)
                {
                    //"p","a","l","h","N"
                    var key_arr = key.Split('~');
                    if (key_arr.Count() == 2)
                    {
                        try
                        {
                            var date = DateTime.Parse(string.Join("-", month_year_.Year, month_year_.Month, key_arr[1]));
                            var model = new UserAttendence
                            {
                                UserId = Int32.Parse(key_arr[0]),
                                IsPresent = fc[key].ToString().Trim().ToUpper() == "P" ? true : false,
                                Date = date,
                                Location = "Admin Office",
                                Remarks = null,
                                LeaveId = GetLeaveId(leave, fc[key].ToString().Trim().ToUpper()),
                                IsApproved = true,
                                Month_Year = month_year,
                                Value = fc[key].ToString().Trim().ToUpper()
                            };
                            list.Add(model);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                Context.UserAttendences.AddRange(list);
                Context.SaveChanges();

                result.message = "SUCCESS";
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
            }
            return result;
        }
        private int? GetLeaveId(IList<MA_Leave> leave, string v)
        {
            if (v == "A" || v == "H")
                return leave.Where(x => x.FromDate == null && x.IsPaid == null).First().Id;
            else if (v == "L")
                return leave.Where(x => x.FromDate == null && x.IsPaid != null).First().Id;
            else
                return null;
        }
        public User_Attendence_Excel_Model Upload(User_Attendence_Excel_Model model)
        {
            var result = new User_Attendence_Excel_Model();
            result.Month_Year = model.Month_Year;
            string filePath = string.Empty;
            if (model.POSTED_CSV != null)
            {
                var COMMAND = "EXEC USP_ATTENDENCE_TEMP_TBL";
                var affectedDatas = Context.Database.ExecuteSqlCommand(COMMAND);

                string path = HttpContext.Current.Server.MapPath("~//Utility//Uploads//Attendence//");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(model.POSTED_CSV.FileName);
                string extension = Path.GetExtension(model.POSTED_CSV.FileName);
                if (extension.ToLower() == ".csv")
                {
                    model.POSTED_CSV.SaveAs(filePath);

                    //Read the contents of CSV file.
                    string csvData = System.IO.File.ReadAllText(filePath);


                    var month_year_ = DateTime.ParseExact(model.Month_Year, "MMM yyyy", CultureInfo.CurrentCulture);
                    int Total_Working_Days = DateTime.DaysInMonth(month_year_.Year, month_year_.Month);
                    result.Total_Working_Days = Total_Working_Days;

                    int id = 0;
                    //Execute a loop over the rows.
                    foreach (string row in csvData.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row) && id != 0)
                        {
                            var UserId = Convert.ToInt32(row.Split(',')[0]);
                            //var Date = Convert.ToInt32(row.Split(',')[1]);
                            //var Value = row.Split(',')[2].Replace("\r", "");

                            for (int column = 1; column <= Total_Working_Days; column++)
                            {
                                int Date = column;
                                string Value = string.Empty;
                                try
                                {
                                    Value = row.Split(',')[column].Replace("\r", "");
                                }
                                catch
                                {
                                    Value = "";
                                }

                                var QUERY = "INSERT INTO TEMP_ATTENDENCE(USERID,DATE,VALUE,MONTH_YEAR) VALUES(" + UserId + "," + Date + ",'" + Value + "','" + model.Month_Year + "')";
                                var affectedROW = Context.Database.ExecuteSqlCommand(QUERY);

                                result.Lst.Add(new User_Attendence_Excel_Model_1
                                {
                                    UserId = UserId,
                                    Date = Date,
                                    Value = Value
                                });
                            }
                        }
                        id += 1;
                    }
                }
            }
            return result;
        }
        public void Save_Csv_Attendence()
        {
            var COMMAND = "EXEC USP_LOAD_ATTENDENCE";
            var affectedDatas = Context.Database.ExecuteSqlCommand(COMMAND);
        }


        //-----------------------Salary---------------
        public IList<UserSalaryModel_Base> GetSalary(string month_Year)
        {
            var result = new List<UserSalaryModel_Base>();
            //try
            //{
            result = (from x in Context.UserSalaries.ToList().Where(x => x.Month_Year == month_Year).OrderByDescending(z => z.Id).GroupBy(y => new { y.UserId, y.Month_Year }).ToList()
                      select new UserSalaryModel_Base
                      {
                          UserId = (int)x.Key.UserId,
                          Name = x.FirstOrDefault().MA_User.Name,
                          Project = String.Join(",", x.FirstOrDefault().MA_User.UserProjects.Select(y => y.MA_Project.ProjectName)),
                          Total_Days_Worked = x.FirstOrDefault().Total_Days_Worked,
                          Loan = x.FirstOrDefault().Loan,
                          Calculated_Salary = x.FirstOrDefault().Calculated_Salary,
                          ESIC = x.FirstOrDefault().ESIC,
                          Cash_Pay = 0,
                          Account_Pay = 0,
                          Remaining_Amount = x.FirstOrDefault().Remaining_Amount,
                          Active_Bank_Account = x.FirstOrDefault().MA_User.UserBankDetails == null ? false : x.FirstOrDefault().MA_User.UserBankDetails.Count() <= 0 ? false : x.FirstOrDefault().MA_User.UserBankDetails.FirstOrDefault().Is_Active == null ? false : (bool)x.FirstOrDefault().MA_User.UserBankDetails.FirstOrDefault().Is_Active
                      }).ToList();
            //}
            //catch (Exception ex)
            //{
            //}
            return result;
        }
        public IList<UserSalaryModel_Base> GenerateSalary(string Month_Year)
        {
            var result = new List<UserSalaryModel_Base>();
            //try
            //{
            var month_year = DateTime.ParseExact(Month_Year, "MMM yyyy", CultureInfo.CurrentCulture);
            int Total_Working_Days = DateTime.DaysInMonth(month_year.Year, month_year.Month);

            var COMMAND = "EXEC USP_ReGenerateSalary '" + Month_Year + "'";
            var affectedDatas = Context.Database.ExecuteSqlCommand(COMMAND);

            var entity = new List<UserSalary>();
            var UserList = this.Get();
            var Paid_UserId = (from a in Context.UserSalaries
                               where a.Month_Year == Month_Year
                               select a.UserId
                                     ).Distinct().ToList();

            foreach (var x in UserList)
            {
                bool Paid = Paid_UserId.Contains(x.Id);
                var UserAttendences = x.UserAttendences.Where(z => z.Month_Year == Month_Year && !string.IsNullOrEmpty(z.Value)).ToList();
                if (!Paid && UserAttendences.Count == Total_Working_Days)
                {
                    var model = Calc(x, UserAttendences, Month_Year, Total_Working_Days);
                    entity.Add(model);
                }
            }
            Context.UserSalaries.AddRange(entity);
            Context.SaveChanges();
            //}
            //catch (Exception ex)
            //{
            //}
            return result;
        }
        public UserSalary Calc(MA_User User, IList<UserAttendence> UserAttendence, string Month_Year, int Total_Working_Days)
        {
            var model = new UserSalary();
            var UserLeaveDetails = this.GetUserLeaveDetails(User.Id);

            model = new UserSalary
            {
                UserId = User.Id,
                Month_Year = Month_Year,
                Annual_Basic_Salary = User.Annual_Basic_Salary,
                Annual_Special_Allowance = User.Annual_Special_Allowance,
                Annual_PF_Amount = User.Annual_PF_Amount,
                CTC = User.Annual_Basic_Salary + User.Annual_Special_Allowance + User.Annual_PF_Amount,
                Monthly_Fixed = (User.Annual_Basic_Salary + User.Annual_Special_Allowance) / 12,
                Total_Working_Days = Total_Working_Days,
                LeaveAllocated = UserLeaveDetails.Leave_Allocated,
                Paid_Leave = UserLeaveDetails.Leave_Taken,
                Remaining_Leave = UserLeaveDetails.Remaining_Leave,
                Remarks = null,
                Paid_On = null,
                ESIC = 0,
                Paid = 0,
                Payment_Mode = "NA",
                IsDraft = "N"
            };

            var HolidayInMonth = MA_Leave_Service.GetAll().ToList().Where(x => !string.IsNullOrEmpty(x.FromDate.ToString()) && Convert.ToDateTime(x.FromDate).Month == Convert.ToDateTime(UserAttendence.First().Date).Month).ToList();

            var UserOnHolidayInMonth = (from x in UserAttendence join y in HolidayInMonth on Convert.ToDateTime(x.Date).Date equals Convert.ToDateTime(y.FromDate).Date select x).ToList();

            var PresentInMonth = UserAttendence.Where(x => x.Value == "P").ToList().Except(UserOnHolidayInMonth).ToList();

            var AbsentInMonth = UserAttendence.Where(x => x.Value == "A").ToList().Except(UserOnHolidayInMonth).ToList();

            var LeaveInMonth = UserAttendence.Where(x => x.Value == "L").ToList().Except(UserOnHolidayInMonth).ToList();

            var P_UserOnHolidayInMonth = UserOnHolidayInMonth.Where(x => x.Value == "P").ToList();
            var A_UserOnHolidayInMonth = UserOnHolidayInMonth.Where(x => x.Value == "A").ToList();
            var H_UserOnHolidayInMonth = UserOnHolidayInMonth.Where(x => x.Value == "H").ToList();
            var L_UserOnHolidayInMonth = UserOnHolidayInMonth.Where(x => x.Value == "L").ToList();



            model.Total_Days_Worked = PresentInMonth.Count() + LeaveInMonth.Count() + P_UserOnHolidayInMonth.Count() + H_UserOnHolidayInMonth.Count() + L_UserOnHolidayInMonth.Count();
            if (UserLeaveDetails.Remaining_Leave <= -1)
                model.Total_Days_Worked = model.Total_Days_Worked + UserLeaveDetails.Remaining_Leave;

            model.UnPaid_Leave = AbsentInMonth.Count();

            model.Loan = Calc_Loan(User.Id);

            //var Carry_Frwd_Salary = Get_Carry_Frwd_Salary(User, Month_Year);

            model.Calculated_Salary = decimal.Round((((decimal)model.Monthly_Fixed / (decimal)model.Total_Working_Days) * (int)model.Total_Days_Worked) - (decimal)model.Loan, 2);
            // model.Calculated_Salary = model.Calculated_Salary;//+ Carry_Frwd_Salary

            model.Remaining_Amount = model.Calculated_Salary;

            return model;
        }
        public decimal Calc_Loan(int UserId, bool IsPaid = false)
        {
            decimal TotalInstallement = 0;
            var MA_Loan_List = MA_Loan_Service.GetUserLoan(UserId);

            foreach (var MA_Loan in MA_Loan_List)
            {
                var UserLoans = MA_Loan.UserLoans.OrderByDescending(a => a.Id).ToList();
                var Amt_Paid_So_Far = UserLoans.Sum(x => x.Amount_Deducted);
                var Remaining_Amount = Amt_Paid_So_Far == 0 ? MA_Loan.Loan - MA_Loan.Installement : MA_Loan.Loan - Amt_Paid_So_Far;

                var InstallmentAmount = Amt_Paid_So_Far == 0 ? MA_Loan.Installement : Remaining_Amount > MA_Loan.Installement ? MA_Loan.Installement : Remaining_Amount;

                TotalInstallement += InstallmentAmount;

                var entity = new UserLoan
                {
                    LoanId = MA_Loan.Id,
                    Amount_Deducted = InstallmentAmount,
                    Remaining_Amount = Remaining_Amount == InstallmentAmount ? 0 : Remaining_Amount,
                    Date = DateTime.Now,
                };

                if (IsPaid)
                {
                    MA_Loan_Service.InsertUserLoan(entity);
                    if (entity.Remaining_Amount == 0)
                    {
                        MA_Loan_Service.DeleteLoan(MA_Loan.Id);
                    }
                }
            }
            return TotalInstallement;
        }

        public DataResult<string> AJAX_Pay_Salary(IList<AJAX_Pay_Salary> model)
        {
            var result = new DataResult<string>();
            foreach (var x in model)
            {
                var usersalary_lst = (from a in Context.UserSalaries where a.Month_Year == x.Month_Year && a.UserId == x.UserId orderby a.Id descending select a).ToList();
                var usersalary = usersalary_lst.FirstOrDefault();
                try
                {
                    var transferId = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
                    usersalary.ESIC = x.ESIC;
                    usersalary.Remarks = x.Remarks;

                    if (x.Cash_Pay > 0)
                    {
                        usersalary.Payment_Mode = "CASH";
                        usersalary.Paid = x.Cash_Pay;
                    }
                    else if (x.Account_Pay > 0)
                    {
                        usersalary.Payment_Mode = "BANK";
                        usersalary.Paid = x.Account_Pay;
                    }
                    else
                    {
                        result.message = "Amount should not be 0";
                        result.StatusCode = APIStatusCode.OK;
                        return result;
                    }

                    usersalary.Remaining_Amount = usersalary.Remaining_Amount - usersalary.Paid;

                    var Pay_Salary = new DataResult<CF_Payouts_RequestTransferModel>();
                    if (usersalary.Payment_Mode == "BANK")
                    {
                        var GetToken = Cashfree_Payouts_Config.GetToken();
                        if (GetToken.subCode == "200")
                        {
                            Pay_Salary = Cashfree_Payouts_Config.RequestTransfer(GetToken.Data, usersalary.MA_User.BeneId, (decimal)usersalary.Paid, transferId);
                        }
                        else
                        {
                            result.message = GetToken.message;
                            result.StatusCode = APIStatusCode.Oops;
                        }
                    }
                    else
                    {
                        Pay_Salary.status = "SUCCESS";
                        Pay_Salary.message = "SUCCESS";
                        Pay_Salary.subCode = "200";
                        Pay_Salary.Data = new CF_Payouts_RequestTransferModel
                        {
                            referenceId = "Admin",
                            utr = "Admin",
                            acknowledged = "Admin"
                        };
                    }

                    if (Pay_Salary.subCode == "200")
                    {
                        usersalary.Paid_On = DateTime.Now;
                        usersalary.CF_referenceId = Pay_Salary.Data.referenceId;
                        usersalary.CF_utr = Pay_Salary.Data.utr;
                        usersalary.CF_subCode = Pay_Salary.subCode;
                        usersalary.CF_status = Pay_Salary.status;
                        usersalary.CF_message = Pay_Salary.message;
                        usersalary.TransferId = transferId;
                        usersalary.Exception = null;
                        usersalary.IsDraft = "N";

                        if (usersalary_lst.Count() == 1)
                        {
                            usersalary.MA_User.Remaining_Leave = usersalary.MA_User.Remaining_Leave <= 0 ? 0 : usersalary.MA_User.Remaining_Leave;

                            decimal UpdatedLoan_Amt = 0;
                            if (usersalary.Loan > 0)
                                UpdatedLoan_Amt = this.Calc_Loan((int)usersalary.UserId, IsPaid: true);
                            if (UpdatedLoan_Amt != usersalary.Loan)
                                usersalary.Exception = "Loan mismatch issue.";
                        }
                    }
                    else
                    {
                        usersalary.Paid_On = DateTime.Now;
                        usersalary.CF_subCode = Pay_Salary.subCode;
                        usersalary.CF_status = Pay_Salary.status;
                        usersalary.CF_message = Pay_Salary.message;
                        usersalary.TransferId = transferId;
                        usersalary.Exception = null;
                        usersalary.IsDraft = "Y";
                        usersalary.Remarks = x.Remarks;
                    }
                    Context.Entry(usersalary).State = EntityState.Added;
                    Context.SaveChanges();
                }
                catch (Exception ex)
                {
                    usersalary.Exception = ex.Message;
                    Context.Entry(usersalary).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                result.message = "SUCCESS";
                result.StatusCode = APIStatusCode.OK;
            }
            return result;
        }
        public DataResult<string> AJAX_View_Details(int UserId, string Month_year)
        {
            var result = new DataResult<string>();
            var lst = (from y in Context.UserSalaries
                       where y.UserId == UserId && y.Month_Year == Month_year
                       select new
                       {
                           Id = y.Id,
                           Name = y.MA_User.Name,
                           Remaining_Amount = y.Remaining_Amount,
                           Paid = y.Paid,
                           Payment_Mode = y.Payment_Mode,
                           CF_message = y.CF_message,
                           CF_referenceId = y.CF_referenceId,
                           CF_status = y.CF_status,
                           CF_subCode = y.CF_subCode,
                           CF_utr = y.CF_utr,
                           Loan = y.Loan,
                           Month_Year = y.Month_Year,
                           Paid_On = y.Paid_On,
                           Remarks = y.Remarks,
                           UserId = y.UserId,
                       }).ToList();
            result.Data = JsonConvert.SerializeObject(lst);
            result.message = "SUCCESS";
            result.StatusCode = APIStatusCode.OK;
            return result;
        }
        public DataResult<string> RefreshTransferStatus(int UserSalaryId)
        {
            var result = new DataResult<string>();
            var usersalary = Context.UserSalaries.Find(UserSalaryId);

            if (usersalary.Payment_Mode == "BANK" && usersalary.CF_subCode != "200")
            {
                var GetToken = Cashfree_Payouts_Config.GetToken();
                var data = Cashfree_Payouts_Config.GetTransferStatus(GetToken.Data, usersalary.TransferId);

                if (data.subCode == "200")
                {
                    var resp = data.Data;
                    usersalary.CF_referenceId = resp.referenceId;
                    usersalary.CF_utr = resp.utr;
                    usersalary.CF_subCode = data.subCode;
                    usersalary.CF_status = resp.status;
                    usersalary.CF_message = data.message;
                    Context.Entry(usersalary).State = EntityState.Modified;
                    Context.SaveChanges();
                }
            }

            result.Data = "SUCCESS";
            result.message = "SUCCESS";
            result.StatusCode = APIStatusCode.OK;
            return result;
        }


        //---------------------Insentive--------------------
        public DataResult<InsentiveModel> AddEditInsentive(InsentiveModel model)
        {
            var result = new DataResult<InsentiveModel>();
            try
            {
                if (model.InsentiveId == 0)
                {
                    var entity = new UserInsentive
                    {
                        UserId = model.UserId,
                        Mode = model.Mode,
                        Amount = model.Amount,
                        Remarks = model.Remarks,
                        Date = DateTime.Now,
                    };
                    Context.UserInsentives.Add(entity);
                    Context.SaveChanges();

                    model.InsentiveId = entity.Id;

                    if (model.Mode != "Cash")
                    {
                        PayInsentive_Bank_Transfer(model.UserId, entity.Id, model.Amount);
                        //model.CF_subCode = cf_data.CF_subCode;
                        //model.CF_status = cf_data.CF_status;
                        //model.CF_message = cf_data.CF_message;
                        //model.TransferId = cf_data.TransferId;
                        //model.CF_referenceId = cf_data.CF_referenceId;
                        //model.CF_utr = cf_data.CF_utr;
                    }

                    result.Data = model;
                    result.StatusCode = APIStatusCode.OK;
                }
                else
                {
                    result.Data = model;
                    result.StatusCode = APIStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public void PayInsentive_Bank_Transfer(int userid, int UserInsentiveId, decimal amount)
        {
            try
            {
                var user = Context.MA_User.Find(userid);
                var UserInsentive = Context.UserInsentives.Find(UserInsentiveId);
                if (user != null && amount > 0)
                {
                    var GetToken = Cashfree_Payouts_Config.GetToken();

                    var transferId = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
                    var Pay_Insentive = new DataResult<CF_Payouts_RequestTransferModel>();

                    if (GetToken.subCode == "200")
                    {
                        Pay_Insentive = Cashfree_Payouts_Config.RequestTransfer(GetToken.Data, user.BeneId, amount, transferId);

                        UserInsentive.CF_subCode = Pay_Insentive.subCode;
                        UserInsentive.CF_status = Pay_Insentive.status;
                        UserInsentive.CF_message = Pay_Insentive.message;
                        UserInsentive.TransferId = transferId;
                    }
                    else
                    {
                        UserInsentive.CF_message = GetToken.message;
                        UserInsentive.CF_subCode = GetToken.subCode;
                        UserInsentive.CF_status = GetToken.status;
                    }

                    if (Pay_Insentive.subCode == "200")
                    {
                        UserInsentive.CF_referenceId = Pay_Insentive.Data.referenceId;
                        UserInsentive.CF_utr = Pay_Insentive.Data.utr;
                    }

                    Context.Entry(UserInsentive).State = EntityState.Modified;
                    Context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
            }
        }
        public IList<UserInsentive> GetAll_UserInsentive()
        {
            return Context.UserInsentives.OrderByDescending(x => x.Id).ToList();
        }
        public InsentiveModel Get_UserInsentive(int Id)
        {
            var model = new InsentiveModel();
            model.UserLst = (from y in this.Get()
                             where y.UserBankDetails.Count > 0 && y.UserBankDetails.First().Is_Active == true
                             select new SelectListItem { Text = y.Name, Value = y.Id.ToString() }).ToList();

            if (Id != 0)
            {
                var x = Context.UserInsentives.Find(Id);
                model = new InsentiveModel
                {
                    UserId = (int)x.UserId,
                    Mode = x.Mode,
                    Amount = (decimal)x.Amount,
                    Remarks = x.Remarks,
                    //CF_subCode = x.CF_subCode,
                    //CF_status = x.CF_status,
                    //CF_message = x.CF_message,
                    //TransferId = x.TransferId,
                    //CF_referenceId = x.CF_referenceId,
                    //CF_utr = x.CF_utr,
                    Date = x.Date
                };
            }
            return model;
        }


    }


}