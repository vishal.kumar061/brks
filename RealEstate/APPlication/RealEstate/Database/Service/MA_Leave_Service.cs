﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RealEstate.Database.Service
{
    public interface IMA_Leave_Service
    {
        IList<MA_Leave> GetAll();
        MA_Leave Get(int id);
        DataResult<string> AddEditLeave(MA_Leave model);
        DataResult<string> DeleteLeave(int id);
    }
    public class MA_Leave_Service : IMA_Leave_Service
    {
        private readonly eApp Context;
        public MA_Leave_Service()
        {
            Context = new eApp();
        }


        public MA_Leave Get(int id)
        {
            return (from x in Context.MA_Leave where x.Id == id select x).FirstOrDefault();
        }

        public IList<MA_Leave> GetAll()
        {
            return (from x in Context.MA_Leave select x).ToList();
        }



        public DataResult<string> AddEditLeave(MA_Leave model)
        {
            var result = new DataResult<string>();
            try
            {
                if (model.Id == 0)
                {
                    Context.MA_Leave.Add(model);
                    Context.SaveChanges();
                }
                else
                {
                    var entity = this.Get(model.Id);
                    entity.Name = model.Name;
                    entity.FromDate = model.FromDate;
                    //entity.ToDate = model.ToDate;
                    entity.IsPaid = model.IsPaid;
                    Context.Entry(entity).State = EntityState.Modified;
                    Context.SaveChanges();
                }

                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<string> DeleteLeave(int id)
        {
            var result = new DataResult<string>();
            try
            {
                var entity = this.Get(id);
                Context.Entry(entity).State = EntityState.Deleted;
                Context.SaveChanges();
                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
    }

}