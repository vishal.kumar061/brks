﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RealEstate.Database.Service
{
    public interface IActionLog_Service
    {
        void Add(MA_ActionLog e);
    }
    public class ActionLog_Service : IActionLog_Service
    {
        private readonly eApp Context;
        public ActionLog_Service()
        {
            Context = new eApp();
        }

        public void Add(MA_ActionLog e)
        {
            Context.MA_ActionLog.Add(e);
            Context.SaveChanges();
        }
        public void Update(MA_ActionLog e)
        {
            Context.Entry(e).State = EntityState.Modified;
            Context.SaveChanges();
        }
        public MA_ActionLog Get(int id)
        {
            return Context.MA_ActionLog.Find(id);
        }
        public IList<MA_ActionLog> Get()
        {
            return Context.MA_ActionLog.ToList();
        }

        public void Delete(int Id)
        {
            var e = this.Get(Id);
            Context.MA_ActionLog.Remove(e);
            Context.SaveChanges();
        }
    }
}