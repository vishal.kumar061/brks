﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Database.Service
{
    public interface IMA_Loan_Service
    {
        MA_Loan Get(int id);
        IList<LoanModel> GetAll();
        IList<LoanModel> GetOverAll();
        DataResult<string> AddEditLoan(LoanModel model);
        DataResult<string> DeleteLoan(int id);
        IList<MA_Loan> GetUserLoan(int Userid);
        DataResult<string> InsertUserLoan(UserLoan model);
    }
    public class MA_Loan_Service : IMA_Loan_Service
    {
        private readonly eApp Context;

        public MA_Loan_Service()
        {
            Context = new eApp();
        }
        public MA_Loan Get(int id)
        {
            return (from x in Context.MA_Loan where x.Id == id && x.IsDeleted == false select x).FirstOrDefault();
        }


        public IList<LoanModel> GetOverAll()
        {
            var data = (from x in Context.MA_Loan
                        select new LoanModel
                        {
                            UserName = x.MA_User.Name,
                            Installement = x.Installement,
                            IsActive = x.IsActive,
                            Loan = x.Loan,
                            LoanId = x.Id,
                            UserId = x.UserId,
                            Mode = x.Mode,
                            CF_message = x.Mode == "Cash" ? "Admin" : x.CF_message,
                            CF_status = x.Mode == "Cash" ? "Admin" : x.CF_status,
                            CF_referenceId = x.Mode == "Cash" ? "Admin" : x.CF_referenceId,
                            CF_subCode = x.Mode == "Cash" ? "Admin" : x.CF_subCode,
                            CF_utr = x.Mode == "Cash" ? "Admin" : x.CF_utr,
                            TransferId = x.Mode == "Cash" ? "Admin" : x.TransferId,
                            IsDeleted = x.IsDeleted,
                            Remaining_Amount = x.UserLoans.Count() > 0 ? x.Loan - x.UserLoans.Sum(z => z.Amount_Deducted) : x.Loan

                        }).OrderByDescending(x => x.IsActive).ToList();
            return data;
        }

        public IList<LoanModel> GetAll()
        {
            var data = (from x in Context.MA_Loan.Where(x => x.IsDeleted == false)
                        select new LoanModel
                        {
                            UserName = x.MA_User.Name,
                            Installement = x.Installement,
                            IsActive = x.IsActive,
                            Loan = x.Loan,
                            LoanId = x.Id,
                            UserId = x.UserId,
                            Mode = x.Mode,
                            CF_message = x.Mode == "Cash" ? "Admin" : x.CF_message,
                            CF_status = x.Mode == "Cash" ? "Admin" : x.CF_status,
                            CF_referenceId = x.Mode == "Cash" ? "Admin" : x.CF_referenceId,
                            CF_subCode = x.Mode == "Cash" ? "Admin" : x.CF_subCode,
                            CF_utr = x.Mode == "Cash" ? "Admin" : x.CF_utr,
                            TransferId = x.Mode == "Cash" ? "Admin" : x.TransferId,
                            IsDeleted = x.IsDeleted,
                            Remaining_Amount = x.UserLoans.Count() > 0 ? x.Loan - x.UserLoans.Sum(z => z.Amount_Deducted) : x.Loan

                        }).OrderByDescending(x => x.IsActive).ToList();
            return data;
        }
        public DataResult<string> AddEditLoan(LoanModel model)
        {
            var result = new DataResult<string>();
            try
            {
                if (model.LoanId == 0)
                {
                    var entity = new MA_Loan
                    {
                        Date = DateTime.Now,
                        Installement = model.Installement,
                        Loan = model.Loan,
                        UserId = model.UserId,
                        Mode = model.Mode,
                        IsActive = true,
                        IsDeleted = false
                    };
                    Context.MA_Loan.Add(entity);
                    Context.SaveChanges();
                    if (model.Mode != "Cash")
                        PayLoan_Bank_Transfer(model.UserId, entity.Id, model.Loan);

                }
                else
                {
                    var earlier_mode = "";
                    var entity = this.Get(model.LoanId);
                    earlier_mode = entity.Mode;

                    entity.Installement = model.Installement;
                    entity.IsActive = model.IsActive;

                    if (earlier_mode == "Cash")
                    {
                        entity.Loan = model.Loan;
                        entity.UserId = model.UserId;
                        entity.Mode = model.Mode;
                        entity.Date = DateTime.Now;
                    }
                    Context.Entry(entity).State = EntityState.Modified;
                    Context.SaveChanges();

                    if (earlier_mode == "Cash" && model.Mode != "Cash")
                        PayLoan_Bank_Transfer(model.UserId, entity.Id, model.Loan);
                }
                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public void PayLoan_Bank_Transfer(int userid, int MA_Loan_Id, decimal amount)
        {
            try
            {
                var user = Context.MA_User.Find(userid);
                var Ma_Loan = Context.MA_Loan.Find(MA_Loan_Id);
                if (user != null && amount > 0)
                {
                    var GetToken = Cashfree_Payouts_Config.GetToken();

                    var transferId = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
                    var Pay_Salary = new DataResult<CF_Payouts_RequestTransferModel>();

                    if (GetToken.subCode == "200")
                    {
                        Pay_Salary = Cashfree_Payouts_Config.RequestTransfer(GetToken.Data, user.BeneId, amount, transferId);

                        Ma_Loan.CF_subCode = Pay_Salary.subCode;
                        Ma_Loan.CF_status = Pay_Salary.status;
                        Ma_Loan.CF_message = Pay_Salary.message;
                        Ma_Loan.TransferId = transferId;
                    }
                    else
                    {
                        Ma_Loan.CF_message = GetToken.message;
                        Ma_Loan.CF_subCode = GetToken.subCode;
                        Ma_Loan.CF_status = GetToken.status;
                    }

                    if (Pay_Salary.subCode == "200")
                    {
                        Ma_Loan.CF_referenceId = Pay_Salary.Data.referenceId;
                        Ma_Loan.CF_utr = Pay_Salary.Data.utr;
                    }
                    else
                    {
                        Ma_Loan.IsActive = false;
                    }

                    Context.Entry(Ma_Loan).State = EntityState.Modified;
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
        }



        public DataResult<string> DeleteLoan(int id)
        {
            var result = new DataResult<string>();
            try
            {
                //var COMMAND = "DELETE FROM USERLOAN WHERE LOANID IN (" + id + ")";
                //var affectedDatas = Context.Database.ExecuteSqlCommand(COMMAND);

                //var COMMAND1 = "DELETE FROM MA_LOAN WHERE ID IN (" + id + ")";
                //var affectedDatas1 = Context.Database.ExecuteSqlCommand(COMMAND1);

                var entity = this.Get(id);
                entity.IsDeleted = true;
                Context.Entry(entity).State = EntityState.Modified;
                Context.SaveChanges();
                result.StatusCode = APIStatusCode.OK;
                result.Data = "SUCCESS";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }

        public IList<MA_Loan> GetUserLoan(int Userid)
        {
            var data = (from x in Context.MA_Loan where x.IsDeleted == false && x.IsActive == true && x.UserId == Userid select x).ToList();
            return data;
        }



        public DataResult<string> InsertUserLoan(UserLoan model)
        {
            var result = new DataResult<string>();
            try
            {
                Context.UserLoans.Add(model);
                Context.SaveChanges();
                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }


    }

}