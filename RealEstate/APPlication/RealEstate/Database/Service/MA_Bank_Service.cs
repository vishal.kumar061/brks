﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RealEstate.Database.Service
{
    public interface IMA_Bank_Service
    {
        IList<MA_Bank> GetAll();
        MA_Bank Get(int id);
        DataResult<string> AddEditBank(MA_Bank entity);
    }
    public class MA_Bank_Service : IMA_Bank_Service
    {
        private readonly eApp Context;
        public MA_Bank_Service()
        {
            Context = new eApp();
        }


        public MA_Bank Get(int id)
        {
            return (from x in Context.MA_Bank where x.Id == id select x).FirstOrDefault();
        }

        public IList<MA_Bank> GetAll()
        {
            return (from x in Context.MA_Bank select x).ToList();
        }
        public DataResult<string> AddEditBank(MA_Bank entity)
        {
            var result = new DataResult<string>();
            try
            {
                if (entity.Id == 0)
                {
                    Context.MA_Bank.Add(entity);
                    Context.SaveChanges();
                }
                else
                {
                    Context.Entry(entity).State = EntityState.Modified;
                    Context.SaveChanges();
                }

                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
    }

}