﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RealEstate.Database.Service
{
    public interface IMA_Project_Service
    {
        IList<MA_Project> GetAll();
        MA_Project Get(int id);
        DataResult<string> AddEditProject(ProjectModel model);
        DataResult<string> DeleteProject(int id);
    }
    public class MA_Project_Service : IMA_Project_Service
    {
        private readonly eApp Context;
        public MA_Project_Service()
        {
            Context = new eApp();
        }


        public MA_Project Get(int id)
        {
            return (from x in Context.MA_Project where x.Id == id && x.IsActive == true select x).FirstOrDefault();
        }

        public IList<MA_Project> GetAll()
        {
            return (from x in Context.MA_Project where x.IsActive == true select x).ToList();
        }



        public DataResult<string> AddEditProject(ProjectModel model)
        {
            var result = new DataResult<string>();
            try
            {
                if (model.Id == 0)
                {
                    var entity = new MA_Project
                    {
                        ProjectName = model.ProjectName,
                        IsActive = true,
                    };
                    Context.MA_Project.Add(entity);
                    Context.SaveChanges();
                }
                else
                {
                    var entity = this.Get(model.Id);
                    entity.ProjectName = model.ProjectName;
                    Context.Entry(entity).State = EntityState.Modified;
                    Context.SaveChanges();
                }

                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<string> DeleteProject(int id)
        {
            var result = new DataResult<string>();
            try
            {
                var entity = this.Get(id);
                //Context.Project_Images.RemoveRange(entity.Project_Images);
                //entity.Properties.ForEach(x => Property_Service.DeleteProperty(x.Id));
                entity.IsActive = false;
                Context.Entry(entity).State = EntityState.Modified;
                Context.SaveChanges();
                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }


    }

}