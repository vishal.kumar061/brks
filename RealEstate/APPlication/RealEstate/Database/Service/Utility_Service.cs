﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Database.Service
{
    public interface IUtility_Service
    {
        IList<MA_Bank> GetBankList();
        IList<MA_Region> GetRegionList();
        //IList<MA_Leave> GetLeaveList();
    }
    public class Utility_Service : IUtility_Service
    {
        private readonly eApp Context;
        public Utility_Service()
        {
            Context = new eApp();
        }

        public IList<MA_Bank> GetBankList()
        {
            return (from x in Context.MA_Bank select x).OrderBy(x => x.BankName).ToList();
        }

        //public IList<MA_Leave> GetLeaveList()
        //{
        //    return (from x in Context.MA_Leave select x).ToList();
        //}

        public IList<MA_Region> GetRegionList()
        {
            return (from x in Context.MA_Region where x.IsActive == true select x).ToList();
        }
    }

}