﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Models;
using RealEstate.Models.API;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RealEstate.Database.Service
{


    public interface IProperty_Service
    {
        void Add(Property e);
        void Update(Property e);
        Property Get(int id);
        IList<Property> Get();
        IList<Property> GetTrendingProperty(int count);
        IList<Property> GetbyUserId(int userid);
        IList<Property> GetProjectProperty();
        IList<Property> GetResellerProperty();


        DataResult<IList<PropertyModel>> GetApprovedProperty();
        DataResult<string> AddEditProperty(PropertyInfoModel model);
        DataResult<string> DeleteProperty(int id);
        DataResult<string> ApproveRejectProperty(int id);
        DataResult<IList<PropertyModel>> Get(bool IsReseller = false, bool IsProject = false);
        DataResult<IList<PropertyModel>> GetByProjectId(int ProjectId);
        DataResult<PropertyModel> GetByPropertyId(int propertyId);
        DataResult<IList<PropertyModel>> GetProperty_rent();
        DataResult<IList<PropertyModel>> GetProperty_sale();
        DataResult<IList<PropertyModel>> search(FilterModel model);
    }
    public class Property_Service : IProperty_Service
    {
        private readonly eApp Context;
        private readonly IMA_Project_Service MA_Project_Service;
        public Property_Service()
        {
            Context = new eApp();
            MA_Project_Service = new MA_Project_Service();
        }

        public void Add(Property e)
        {
            Context.Properties.Add(e);
            Context.SaveChanges();
        }
        public void Update(Property e)
        {
            Context.Entry(e).State = EntityState.Modified;
            Context.SaveChanges();
        }
        public Property Get(int id)
        {
            return Context.Properties.Find(id);
        }

        public IList<Property> Get()
        {
            return (from x in Context.Properties where x.IsActive == true orderby x.CreatedOn descending select x).ToList();
        }
        public IList<Property> GetProjectProperty()
        {
            return (from x in Context.Properties where x.IsActive == true && x.Property_Category_Id_F == 20 orderby x.CreatedOn descending select x).ToList();
        }

        public IList<Property> GetResellerProperty()
        {
            return (from x in Context.Properties where x.IsActive == true && x.Property_Category_Id_F == 19 orderby x.CreatedOn descending select x).ToList();
        }
        public IList<Property> GetTrendingProperty(int count)
        {
            return (from x in Context.Properties where x.IsActive == true && x.IsApproved == true && x.Property_Images.Count > 0 orderby x.CreatedOn descending select x).Take(count).ToList();
        }
        public IList<Property> GetbyUserId(int userid)
        {
            return (from x in Context.Properties where x.IsActive == true && x.CreatedBy == userid select x).ToList();
        }

        public DataResult<string> AddEditProperty(PropertyInfoModel model)
        {
            var _projectIdOfNone = 0;
            var project = MA_Project_Service.Get("None");
            if (project == null)
            {
                var entity = new MA_Project
                {
                    ProjectName = "None",
                    IsActive = true,
                };
                _projectIdOfNone = MA_Project_Service.Add(entity);
            }
            else
                _projectIdOfNone = project.Id;



            var result = new DataResult<string>();
            try
            {
                if (model.Id == 0)
                {
                    var entity = new Property
                    {
                        Property_Category_Id_F = model.Property_Category_Id_F,
                        Project_Id = model.Property_Category_Id_F == 20 ? model.Project_Id : _projectIdOfNone,


                        Address = model.Address,
                        Amenities = string.Join(",", model.Amenities),
                        BHK_Id_F = model.BHK_Id_F,
                        BookingAmount = model.BookingAmount,
                        Carpet_Area = model.Carpet_Area,
                        //CountryId = model.CountryId,
                        Cover_Area = model.Cover_Area,
                        CreatedOn = DateTime.Now,
                        CurrentFloor = model.CurrentFloor,
                        Facing_Id_F = model.Facing_Id_F,
                        Firnishing_Id_F = model.Firnishing_Id_F,
                        IsActive = true,
                        IsApproved = model.IsAdmin ? true : false,
                        MaintenanceCharge = model.MaintenanceCharge,
                        TotalFloor = model.TotalFloor,
                        PinCode = model.PinCode,
                        Price = model.Price,
                        PropertyType_Id_F = model.PropertyType_Id_F,
                        Property_Images = model.Files.FirstOrDefault() != null ? (from x in model.modified_file_name.ToList() select new Property_Images { ImageName = x, IsActive = true }).ToList() : null,
                        StateId = model.StateId,
                        Status_Id_F = model.Status_Id_F,
                        CreatedBy = model.CreatedBy,
                        Title = model.Title,
                        Society = model.Society,
                        Configuration = model.Configuration

                    };
                    Context.Properties.Add(entity);
                    Context.SaveChanges();
                }
                else
                {
                    var entity = this.Get(model.Id);
                    entity.Property_Category_Id_F = model.Property_Category_Id_F;
                    entity.Project_Id = model.Property_Category_Id_F == 20 ? model.Project_Id : _projectIdOfNone;




                    entity.Address = model.Address;
                    entity.Amenities = string.Join(",", model.Amenities);
                    entity.BHK_Id_F = model.BHK_Id_F;
                    entity.BookingAmount = model.BookingAmount;
                    entity.Carpet_Area = model.Carpet_Area;
                    entity.Cover_Area = model.Cover_Area;
                    entity.UpdatedOn = DateTime.Now;
                    entity.CurrentFloor = model.CurrentFloor;
                    entity.Facing_Id_F = model.Facing_Id_F;
                    entity.Firnishing_Id_F = model.Firnishing_Id_F;
                    entity.IsActive = true;
                    entity.IsApproved = model.IsAdmin ? true : false;
                    entity.MaintenanceCharge = model.MaintenanceCharge;
                    entity.TotalFloor = model.TotalFloor;
                    entity.PinCode = model.PinCode;
                    entity.Price = model.Price;
                    entity.PropertyType_Id_F = model.PropertyType_Id_F;
                    entity.StateId = model.StateId;
                    entity.Status_Id_F = model.Status_Id_F;
                    entity.CreatedBy = model.CreatedBy;
                    entity.Title = model.Title;
                    entity.Configuration = model.Configuration;
                    entity.Society = model.Society;


                    entity.Property_Images = model.Files.FirstOrDefault() != null ? (from x in model.modified_file_name.ToList() select new Property_Images { ImageName = x, IsActive = true }).ToList() : null;

                    Context.Entry(entity).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }

        public DataResult<string> DeleteProperty(int id)
        {
            var result = new DataResult<string>();
            try
            {
                var entity = this.Get(id);
                Context.Property_Images.RemoveRange(entity.Property_Images);
                Context.Properties.Remove(entity);
                Context.SaveChanges();
                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<string> ApproveRejectProperty(int id)
        {
            var result = new DataResult<string>();
            try
            {
                var entity = this.Get(id);
                entity.IsApproved = entity.IsApproved ? false : true;
                Context.SaveChanges();
                result.StatusCode = APIStatusCode.OK;
                result.Data = "OK";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<IList<PropertyModel>> GetApprovedProperty()
        {
            var result = new DataResult<IList<PropertyModel>>();
            try
            {
                var data = ((from x in Context.Properties where x.IsActive == true && x.IsApproved == true orderby x.CreatedOn descending select x).ToList());
                result.Data = this.ToModel(data);
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<IList<PropertyModel>> Get(bool IsReseller = false, bool IsProject = false)
        {
            var result = new DataResult<IList<PropertyModel>>();
            try
            {
                if (IsReseller)
                {
                    var ResellerProperty = this.GetResellerProperty();
                    result.Data = this.ToModel(ResellerProperty);
                    result.StatusCode = APIStatusCode.OK;
                }
                if (IsProject)
                {
                    var ProjectProperty = this.GetProjectProperty();
                    result.Data = this.ToModel(ProjectProperty);
                    result.StatusCode = APIStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<IList<PropertyModel>> GetByProjectId(int ProjectId)
        {
            var result = new DataResult<IList<PropertyModel>>();
            try
            {
                var Property = (from x in Context.Properties where x.Project_Id == ProjectId && x.IsActive == true && x.IsApproved == true select x).ToList();
                result.Data = this.ToModel(Property);
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
        public DataResult<IList<PropertyModel>> GetProperty_rent()
        {
            var result = new DataResult<IList<PropertyModel>>();
            try
            {
                var Property = (from x in Context.Properties where x.MA_ConfigFilter4.FilterValue == "Rent" && x.IsActive == true && x.IsApproved == true select x).ToList();
                result.Data = this.ToModel(Property);
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }

        public DataResult<IList<PropertyModel>> GetProperty_sale()
        {
            var result = new DataResult<IList<PropertyModel>>();
            try
            {
                var Property = (from x in Context.Properties where x.MA_ConfigFilter4.FilterValue == "Sale" && x.IsActive == true && x.IsApproved == true && x.MA_ConfigFilter3.FilterValue.Trim().ToLower() == "reseller property" select x).ToList();
                result.Data = this.ToModel(Property);
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }


        public DataResult<PropertyModel> GetByPropertyId(int propertyId)
        {
            var result = new DataResult<PropertyModel>();
            try
            {
                var Property = (from x in Context.Properties where x.Id == propertyId && x.IsActive == true && x.IsApproved == true select x).ToList();
                result.Data = this.ToModel(Property).FirstOrDefault();
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }

        public IList<PropertyModel> ToModel(IList<Property> entity)
        {
            return (from x in entity
                    select new PropertyModel
                    {
                        ProjectId = x.Project_Id,
                        Address = x.Address,
                        Amenities = x.Amenities,
                        BookingAmount = x.BookingAmount,
                        Carpet_Area = x.Carpet_Area,
                        Cover_Area = x.Cover_Area,
                        CurrentFloor = x.CurrentFloor,
                        Id = x.Id,
                        IsApproved = x.IsApproved,
                        MaintenanceCharge = x.MaintenanceCharge,
                        PinCode = x.PinCode,
                        Price = x.Price,
                        Project_Name = x.MA_Project.ProjectName,
                        Title = x.Title,
                        TotalFloor = x.TotalFloor,

                        BHK = x.MA_ConfigFilter.FilterValue,
                        Facing = x.MA_ConfigFilter1.FilterValue,
                        Firnishing = x.MA_ConfigFilter2.FilterValue,
                        PropertyType = x.MA_ConfigFilter3.FilterValue,
                        Property_Type = x.MA_ConfigFilter4.FilterValue,
                        Status = x.MA_ConfigFilter5.FilterValue,
                        State = x.MA_Region.StateName,
                        Configuration = x.Configuration,
                        Society = x.Society,
                        Property_Images = (from z in x.Property_Images
                                           select new Property_ImageModel
                                           {
                                               Name = z.ImageName,
                                               URL = "http://calci.in/Uploads/PropertyImages/" + z.ImageName
                                           }).ToList()
                    }).ToList();
        }

        public DataResult<IList<PropertyModel>> search(FilterModel model)
        {
            var result = new DataResult<IList<PropertyModel>>();
            try
            {
                var FinalDoclst = new List<Property>();

                //Property_Type = new List<Property_Type_Model>();
                //Location = new List<Location_model>();
                //Title = new List<Title_model>();
                //BHK = new List<BHK_model>();
                //Sqft = new List<Sqft_model>();
                //MaxPrice = new List<MaxPrice_model>();
                //MinPrice = new List<MinPrice_model>();



                if (!string.IsNullOrEmpty(model.Property_Type)
                   && !string.IsNullOrEmpty(model.Location)
                   && !string.IsNullOrEmpty(model.Title)
                   && model.BHK.Count == 0//!string.IsNullOrEmpty(model.BHK)

                   && !string.IsNullOrEmpty(model.Property_Type)
                   && !string.IsNullOrEmpty(model.Property_Type)
                   && model.MinSqft > 0
                   && model.MaxSqft > 0
                   && model.MaxPrice != null && model.MaxPrice > 0
                   && model.MinPrice != null && model.MinPrice > 0)
                {
                    result.Data = this.ToModel(this.Get());
                    result.StatusCode = APIStatusCode.OK;
                    return result;
                }

                var All_property = this.Get().ToList();

                if (!string.IsNullOrEmpty(model.Property_Type))
                    All_property = All_property.Where(x => x.MA_ConfigFilter4.FilterValue.ToLower().Trim().Contains(model.Property_Type.ToLower().Trim())).ToList();





                if (!string.IsNullOrEmpty(model.Location))
                    All_property = All_property.Where(
                        x =>
                        (!string.IsNullOrEmpty(x.Address) ? x.Address.ToLower().Trim().Contains(model.Location.ToLower().Trim()) : "".ToLower().Trim().Contains(model.Location.ToLower().Trim())) ||

                         (!string.IsNullOrEmpty(x.Society) ? x.Society.ToLower().Trim().Contains(model.Location.ToLower().Trim()) : "".ToLower().Trim().Contains(model.Location.ToLower().Trim())) ||

                         (!string.IsNullOrEmpty(x.MA_Region.StateName) ? x.MA_Region.StateName.ToLower().Trim().Contains(model.Location.ToLower().Trim()) : "".ToLower().Trim().Contains(model.Location.ToLower().Trim()))

                        ).ToList();


                if (!string.IsNullOrEmpty(model.Title))
                    All_property = All_property.Where(x => x.Title.ToLower().Trim().Contains(model.Title.ToLower().Trim())).ToList();

                //if (!string.IsNullOrEmpty(model.BHK))
                //    All_property = All_property.Where(x => x.MA_ConfigFilter.FilterValue.ToLower().Trim().Contains(model.BHK.ToLower().Trim())).ToList();


                //if (model.BHK.Count > 0)
                //    All_property = All_property.Where(x => model.BHK.Select(a => a.BHK.ToLower().Trim()).Contains(x.MA_ConfigFilter.FilterValue.ToLower().Trim())).ToList();

                if (model.BHK.Count > 0)
                    All_property = All_property.Where(x => model.BHK.Select(a => a.BHK.ToLower().Trim()).Contains(x.MA_ConfigFilter.FilterValue.ToLower().Trim())).ToList();



                //if (!string.IsNullOrEmpty(model.Sqft))
                //    All_property = All_property.Where(
                //        x => x.Cover_Area.ToString().ToLower().Trim().Contains(model.Sqft.ToLower().Trim())

                //        //||
                //        //x.Carpet_Area.ToString().ToLower().Trim().Contains(model.Sqft.ToLower().Trim())

                //        ).ToList();


                if (model.MaxSqft > 0)
                    All_property = All_property.Where(x => x.Cover_Area <= model.MaxSqft).ToList();

                if (model.MinSqft > 0)
                    All_property = All_property.Where(x => x.Cover_Area >= model.MinSqft).ToList();




                if (model.MaxPrice != null && model.MaxPrice > 0)
                    All_property = All_property.Where(x => x.Price <= model.MaxPrice).ToList();

                if (model.MinPrice != null && model.MinPrice > 0)
                    All_property = All_property.Where(x => x.Price >= model.MinPrice).ToList();


                result.Data = this.ToModel(All_property);
                result.StatusCode = APIStatusCode.OK;
                return result;



                //----------------------------------

                //if (model.Property_Type.Count == 0
                //    && model.Location.Count == 0
                //    && model.Title.Count == 0
                //    && model.BHK.Count == 0
                //    && model.Sqft.Count == 0
                //    && model.MaxPrice != null && model.MaxPrice > 0
                //     && model.MinPrice != null && model.MinPrice > 0)
                //{
                //    result.Data = this.ToModel(this.Get());
                //    result.StatusCode = APIStatusCode.OK;
                //    return result;
                //}

                //var All_property = this.Get().ToList();

                //if (model.Property_Type.Count > 0)
                //    All_property = All_property.Where(x => model.Property_Type.Select(a => a.Property_Type.ToLower().Trim()).Contains(x.MA_ConfigFilter4.FilterValue.ToLower().Trim())).ToList();

                //if (model.Location.Count > 0)
                //    All_property = All_property.Where(x => model.Location.Select(a => a.Location.ToLower().Trim()).Contains(x.Address.ToLower().Trim())).ToList();
                //if (model.Location.Count > 0)
                //    All_property = All_property.Where(x => model.Location.Select(a => a.Location.ToLower().Trim()).Contains(x.MA_Region.StateName.ToLower().Trim())).ToList();
                //if (model.Location.Count > 0)
                //    All_property = All_property.Where(x => model.Location.Select(a => a.Location.ToLower().Trim()).Contains(x.Society.ToLower().Trim())).ToList();

                //if (model.Title.Count > 0)
                //    All_property = All_property.Where(x => model.Title.Select(a => a.Title.ToLower().Trim()).Contains(x.Title.ToLower().Trim())).ToList();

                //if (model.BHK.Count > 0)
                //    All_property = All_property.Where(x => model.BHK.Select(a => a.BHK.ToLower().Trim()).Contains(x.MA_ConfigFilter.FilterValue.ToLower().Trim())).ToList();

                //if (model.Sqft.Count > 0)
                //    All_property = All_property.Where(x => model.Sqft.Select(a => a.Sqft.ToLower().Trim()).Contains(x.Cover_Area.ToString
                //        ().ToLower().Trim())).ToList();
                //if (model.Sqft.Count > 0)
                //    All_property = All_property.Where(x => model.Sqft.Select(a => a.Sqft.ToLower().Trim()).Contains(x.Carpet_Area.ToString
                //        ().ToLower().Trim())).ToList();

                //if (model.MaxPrice != null && model.MaxPrice > 0)
                //    All_property = All_property.Where(x => x.Price <= model.MaxPrice).ToList();

                //if (model.MinPrice != null && model.MinPrice > 0)
                //    All_property = All_property.Where(x => x.Price >= model.MinPrice).ToList();

                ////var searchedIds = All_property.Select(a => new { Id = a.Id }).Distinct().ToList();
                ////FinalDoclst = this.Get().Where(x => searchedIds.Select(a => a.Id).Contains(x.Id)).OrderByDescending(x => x.CreatedOn).ToList();


                //result.Data = this.ToModel(All_property);
                //result.StatusCode = APIStatusCode.OK;
                //return result;
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
    }
}