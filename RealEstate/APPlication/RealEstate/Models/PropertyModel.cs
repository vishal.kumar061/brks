﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class PropertyModel
    {
        public PropertyModel()
        {
            Property_Images = new HashSet<Property_ImageModel>();
        }

        public int Id { get; set; }
        public int ProjectId { get; set; }

        public string Title { get; set; }

        public string Project_Name { get; set; }

        public string State { get; set; }

        public string Address { get; set; }

        public int? PinCode { get; set; }

        public double? Price { get; set; }

        public double? Cover_Area { get; set; }

        public double? Carpet_Area { get; set; }

        public string BHK { get; set; }

        public string Status { get; set; }

        public string Firnishing { get; set; }

        public string Facing { get; set; }
        public string Property_Type { get; set; }

        public int? TotalFloor { get; set; }

        public int? CurrentFloor { get; set; }

        public double? MaintenanceCharge { get; set; }

        public double? BookingAmount { get; set; }

        public string PropertyType { get; set; }

        public bool IsApproved { get; set; }

        public string Amenities { get; set; }

        public virtual ICollection<Property_ImageModel> Property_Images { get; set; }

        public string Configuration { get; set; }
        public string Society { get; set; }
    }

    public class Property_ImageModel
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }

}