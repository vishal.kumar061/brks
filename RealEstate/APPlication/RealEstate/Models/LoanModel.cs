﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Models
{
    public class LoanModel
    {
        public int LoanId { get; set; }

        [DisplayName("Select User")]
        public IEnumerable<SelectListItem> UserLst { get; set; }
        [Required]
        public int UserId { get; set; }
        public string UserName { get; set; }
        [Required]
        [Range(100, int.MaxValue, ErrorMessage = "Enter a value bigger than 100")]
        public decimal Loan { get; set; }
        [Required]
        [Range(10, int.MaxValue, ErrorMessage = "Enter a value bigger than 10")]
        public decimal Installement { get; set; }
        [Required]
        public bool IsActive { get; set; }

        public string Mode { get; set; }
        public string CF_subCode { get; set; }
        public string CF_status { get; set; }
        public string CF_message { get; set; }
        public string TransferId { get; set; }
        public string CF_referenceId { get; set; }
        public string CF_utr { get; set; }
        public decimal Remaining_Amount { get; set; }


        public bool IsDeleted { get; set; }

    }
}