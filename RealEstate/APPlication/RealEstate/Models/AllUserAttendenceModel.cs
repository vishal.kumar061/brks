﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class AllUserAttendenceModel
    {

        public AllUserAttendenceModel()
        {
            AllUserAttendenceModel_List = new List<AllUserAttendenceModel_Base>();
        }
        public IList<AllUserAttendenceModel_Base> AllUserAttendenceModel_List { get; set; }

        [DisplayName("Select Date")]
        public string Request_Date { get; set; }
    }
    public class AllUserAttendenceModel_Base
    {
        public AllUserAttendenceModel_Base()
        {
            UserLeaveDetail = new UserLeaveDetailsModel();
        }
        public int Id { get; set; }
        public string Name { get; internal set; }
        public bool IsUserPresent { get; internal set; }
        public UserLeaveDetailsModel UserLeaveDetail { get; internal set; }
    }

}
