﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class OTP_GetModel
    {
        [DisplayName("Mobile Number")]
        [Required(ErrorMessage = "Required")]
        [RegularExpression("^\\+[1-9]{1}[0-9]{3,14}$", ErrorMessage = "Country Code missing")]
        public string Mob { get; set; }
    }
    public class OTP_ValidateModel
    {
        [DisplayName("Mobile Number")]
        [Required(ErrorMessage = "Required")]
        [RegularExpression("^\\+[1-9]{1}[0-9]{3,14}$", ErrorMessage = "Country Code missing")]
        public string Mob { get; set; }


        [DisplayName("Message")]
        [Required(ErrorMessage = "Required")]
        public string OTP { get; set; }
    }
}