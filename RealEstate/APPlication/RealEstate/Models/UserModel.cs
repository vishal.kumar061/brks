﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Models
{
    public class UserModel
    {
        public UserModel()
        {
            RoleLst = new List<SelectListItem>();
            BankLst = new List<SelectListItem>();
            UserLeaveDetail = new UserLeaveDetailsModel();
            ProfileImage_Name = "noImg.png";
            AddBeneficiary = new DataResult<string>();
            User = new MA_User();
        }
        public MA_User User { get; set; }
        public int Id { get; set; }
        public bool IsAdmin { get; set; }
        [DisplayName("Name")]
        [Required(ErrorMessage = "Required")]
        public string Name { get; set; }
        [DisplayName("Username")]
        //[Required(ErrorMessage = "Required")]
        public string Username { get; set; }
        //[Required(ErrorMessage = "Required")]
        public string Password { get; set; }
        [DisplayName("Email")]
        //[Required(ErrorMessage = "Required")]
        public string Email { get; set; }

        //[DisplayName("Mobile (With Country Code)")]
        [DisplayName("Phone")]
        //[RegularExpression("^\\+[1-9]{1}[0-9]{3,14}$", ErrorMessage = "Country Code missing")]
        //[Required(ErrorMessage = "Required")]
        public string Mobile { get; set; }
        public string Designation { get; set; }



        [DisplayName("Select Role")]
        public IEnumerable<SelectListItem> RoleLst { get; set; }
        [Required(ErrorMessage = "Required")]
        public IEnumerable<string> RoleId { get; set; }

        [DisplayName("Upload Image")]
        public HttpPostedFileBase ProfileImage { get; set; }
        public string ProfileImage_Name { get; set; }


        //salary details
        [DisplayName("Basic Salary")]
        public decimal? Monthly_Basic_Salary { get; set; }
        [DisplayName("Special Allowance")]
        public decimal? Monthly_Special_Allowance { get; set; }
        [DisplayName("PF")]
        public decimal? Monthly_PF_Amount { get; set; }
        [DisplayName("ESIC")]
        public decimal? Monthly_ESIC { get; set; }

        [DisplayName("CTC")]
        public decimal? Annual_CTC
        {
            get
            {
                return (Monthly_Basic_Salary + Monthly_Special_Allowance + Monthly_PF_Amount + Monthly_ESIC) * 12;
            }
        }

        [DisplayName("Salary")]
        public decimal? Monthly_Salary
        {
            get
            {
                return Monthly_Basic_Salary + Monthly_Special_Allowance + Monthly_PF_Amount + Monthly_ESIC;
            }
        }


        [DisplayName("PAN")]
        public string PAN { get; set; }

        [DisplayName("Aadhar")]
        public string Aadhar { get; set; }

        [DisplayName("UAN No")]
        public string UAN_No { get; set; }

        [DisplayName("PF No")]
        public string PF_No { get; set; }


        //Bank Details

        [DisplayName("Select Bank")]
        public IEnumerable<SelectListItem> BankLst { get; set; }
        public int? BankId { get; set; }
        public string Account_Number { get; set; }
        public string IFSC { get; set; }
        public string UPI { get; set; }

        //Attendence
        //[DisplayName("Annual Leave Allocated")]
        //public int? LeaveAllocated { get; set; }

        public UserLeaveDetailsModel UserLeaveDetail { get; set; }




        //[DisplayName("Remaining Leave")]
        //public int? Remaining_Leave { get; set; }


        //Project
        [DisplayName("Select Project")]
        public IEnumerable<SelectListItem> ProjectLst { get; set; }
        // [Required(ErrorMessage = "Required")]
        public IEnumerable<string> ProjectId { get; set; }



        public DataResult<string> AddBeneficiary { get; set; }
    }


}