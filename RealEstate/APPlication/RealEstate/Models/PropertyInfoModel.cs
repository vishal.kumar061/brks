﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Models
{
    public class PropertyInfoModel
    {

        public PropertyInfoModel()
        {
            ProjectLst = new List<SelectListItem>();
            StateLst = new List<SelectListItem>();
            BHKLst = new List<SelectListItem>();
            StatusLst = new List<SelectListItem>();
            FirnishingLst = new List<SelectListItem>();
            FacingLst = new List<SelectListItem>();
            AmenitiesLst = new List<SelectListItem>();
            PropertyTypeLst = new List<SelectListItem>();
            Amenities = new List<string>();
            modified_file_name = new List<string>();
        }
        public int Id { get; set; }


        [DisplayName("Title")]
        [Required(ErrorMessage = "Required")]
        public string Title { get; set; }

        [DisplayName("Select Project")]
        public IEnumerable<SelectListItem> ProjectLst { get; set; }
        public int Project_Id { get; set; }


        [DisplayName("Select State")]
        public IEnumerable<SelectListItem> StateLst { get; set; }
        public int? StateId { get; set; }

        [DisplayName("No. of BHK")]
        public IEnumerable<SelectListItem> BHKLst { get; set; }
        public int? BHK_Id_F { get; set; }

        [DisplayName("Current Status")]
        public IEnumerable<SelectListItem> StatusLst { get; set; }
        public int? Status_Id_F { get; set; }

        [DisplayName("Firnishing")]
        public IEnumerable<SelectListItem> FirnishingLst { get; set; }
        public int? Firnishing_Id_F { get; set; }

        [DisplayName("Facing")]
        public IEnumerable<SelectListItem> FacingLst { get; set; }
        public int? Facing_Id_F { get; set; }

        [DisplayName("Choose Multiple Amenities")]
        public IEnumerable<SelectListItem> AmenitiesLst { get; set; }
        public IEnumerable<string> Amenities { get; set; }

        [DisplayName("Property Type")]
        public IEnumerable<SelectListItem> PropertyTypeLst { get; set; }
        public int? PropertyType_Id_F { get; set; }

        [DisplayName("Property Category")]
        public IEnumerable<SelectListItem> PropertyCategoryLst { get; set; }
        public int? Property_Category_Id_F { get; set; }




        [DisplayName("Address")]
        public string Address { get; set; }

        [DisplayName("Pin Code")]
        public int? PinCode { get; set; }

        [DisplayName("Price")]
        public double? Price { get; set; }

        [DisplayName("Cover Area (sqft)")]
        public double? Cover_Area { get; set; }

        [DisplayName("Carpet Area (sqft)")]
        public double? Carpet_Area { get; set; }

        [DisplayName("Total Floor")]
        public int? TotalFloor { get; set; }

        [DisplayName("Current Floor")]
        public int? CurrentFloor { get; set; }

        [DisplayName("Maintenance Charge")]
        public double? MaintenanceCharge { get; set; }

        [DisplayName("Booking Amount")]
        public double? BookingAmount { get; set; }

        [DisplayName("Is Approved")]
        public bool IsApproved { get; set; }

        public HttpPostedFileBase[] Files { get; set; }
        public int CreatedBy { get; set; }
        public IList<string> modified_file_name { get; set; }



        public bool IsAdmin { get; set; }




        [DisplayName("Configuration")]
        public string Configuration { get; set; }

        [DisplayName("Society")]
        public string Society { get; set; }


        public bool IsprojectProperty { get; set; }
    }
}