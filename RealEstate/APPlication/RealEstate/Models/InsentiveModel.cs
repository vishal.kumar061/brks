﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Models
{
    public class InsentiveModel
    {
        public int InsentiveId { get; set; }
        [DisplayName("Select User")]
        public IEnumerable<SelectListItem> UserLst { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Enter a value bigger than 1")]
        public decimal Amount { get; set; }
        public string Mode { get; set; }
        public string Remarks { get; set; }
        public DateTime? Date { get; set; }

        //public string CF_subCode { get; set; }
        //public string CF_status { get; set; }
        //public string CF_message { get; set; }
        //public string TransferId { get; set; }
        //public string CF_referenceId { get; set; }
        //public string CF_utr { get; set; }

    }
}