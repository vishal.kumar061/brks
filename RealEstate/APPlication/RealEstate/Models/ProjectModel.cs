﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class ProjectModel
    {
        public ProjectModel()
        {
            //modified_file_name = new List<string>();
        }
        public int Id { get; set; }

        [DisplayName("Project Name")]
        [Required(ErrorMessage = "Required")]
        public string ProjectName { get; set; }

        //[DisplayName("Title")]
        //[Required(ErrorMessage = "Project Title")]
        //public string ProjectTitle { get; set; }

        //[DisplayName("Select Images")]
        //public HttpPostedFileBase[] Files { get; set; }
        //public IList<string> modified_file_name { get; set; }
    }
}