﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class Web_IndexModel
    {
        public IList<Property> Property { get; set; }
    }
}