﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class User_Attendence_Excel_Model
    {
        public User_Attendence_Excel_Model()
        {
            Lst = new List<User_Attendence_Excel_Model_1>();
        }
        public IList<User_Attendence_Excel_Model_1> Lst { get; set; }
        [Required]
        public string Month_Year { get; set; }
        [Required, DisplayName("Upload CSV")]
        public HttpPostedFileBase POSTED_CSV { get; set; }

        public int Total_Working_Days { get; set; }
    }
    public class User_Attendence_Excel_Model_1
    {
        public int UserId { get; set; }
        public int Date { get; set; }
        public string Value { get; set; }
    }
}