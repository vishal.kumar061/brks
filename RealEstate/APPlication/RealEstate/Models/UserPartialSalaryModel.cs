﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class UserPartialSalaryModel
    {
        public UserPartialSalaryModel()
        {
            UserPartialSalaryLst = new List<UserPartialSalaryModel_Base>();
            UserLst = new List<UserPartialSalaryModel_Base>();
        }
        public string Month_Year { get; set; }
        public IList<UserPartialSalaryModel_Base> UserPartialSalaryLst { get; set; }
        public IList<UserPartialSalaryModel_Base> UserLst { get; set; }
    }

    public class UserPartialSalaryModel_Base
    {
        public int UserPartialSalaryId { get; set; }
        public int UserId { get; set; }
        public string BeneId { get; set; }
        public string Name { get; set; }
        public string Project { get; set; }
        public decimal? Amount { get; set; }
        public string CF_referenceId { get; set; }
        public string CF_message { get; set; }
        public bool Active_Bank_Account { get; set; }
        public string Month_Year { get; set; }
    }
}