﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class FilterModel
    {
        public FilterModel()
        {
            //Property_Type = new List<Property_Type_Model>();
            //Location = new List<Location_model>();
            //Title = new List<Title_model>();
            //BHK = new List<BHK_model>();
            //Sqft = new List<Sqft_model>();
            //MaxPrice = new List<MaxPrice_model>();
            //MinPrice = new List<MinPrice_model>();
        }
        //public IList<Property_Type_Model> Property_Type { get; set; }
        //public IList<Location_model> Location { get; set; }
        //public IList<Title_model> Title { get; set; }
        public IList<BHK_model> BHK { get; set; }
        //public IList<Sqft_model> Sqft { get; set; }
        //public IList<MaxPrice_model> MaxPrice { get; set; }

        //public IList<MinPrice_model> MinPrice { get; set; }



        public string Property_Type { get; set; }
        public string Location { get; set; }
        public string Title { get; set; }
        //public string BHK { get; set; }
        public int MinSqft { get; set; }
        public int MaxSqft { get; set; }
        public double? MaxPrice { get; set; }
        public double? MinPrice { get; set; }
    }
    //public class Property_Type_Model
    //{
    //    public string Property_Type { get; set; }
    //}
    //public class Location_model
    //{
    //    public string Location { get; set; }
    //}
    //public class Title_model
    //{
    //    public string Title { get; set; }
    //}
    public class BHK_model
    {
        public string BHK { get; set; }
    }
    //public class Sqft_model
    //{
    //    public string Sqft { get; set; }
    //}
    //public class MaxPrice_model
    //{
    //    public string MaxPrice { get; set; }
    //}
    //public class MinPrice_model
    //{
    //    public string MinPrice { get; set; }
    //}
}