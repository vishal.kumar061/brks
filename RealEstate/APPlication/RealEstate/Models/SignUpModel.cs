﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class SignUpModel
    {
        [DisplayName("Name")]
        [Required(ErrorMessage = "Required")]
        public string Name { get; set; }

        [DisplayName("Email")]
        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [DisplayName("Phone")]
        [Required(ErrorMessage = "Required")]
        [RegularExpression("^\\+[1-9]{1}[0-9]{3,14}$", ErrorMessage = "Country Code missing")]
        public string Phone { get; set; }
    }
}