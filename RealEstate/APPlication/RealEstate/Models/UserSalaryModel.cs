﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Models
{
    public class UserSalaryModel
    {
        public UserSalaryModel()
        {
            UserSalary = new List<UserSalaryModel_Base>();
        }

        public string Month_Year { get; set; }
        public IList<UserSalaryModel_Base> UserSalary { get; set; }
        public decimal Total_Amount { get; set; }
        //public bool Regenerate { get; set; }
    }
    public class UserSalaryModel_Base
    {
        public UserSalaryModel_Base()
        {
            Paid_Leave = 0;
            UnPaid_Leave = 0;
            Remaining_Leave = 0;
            Total_Days_Worked = 0;
            Loan = 0;
            Cash_Pay = 0;
            Account_Pay = 0;
        }
        public int UserSalaryId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Project { get; set; }
        public decimal? Monthly_Basic_Salary { get; set; }
        public decimal? Monthly_Special_Allowance { get; set; }
        public decimal? Monthly_PF_Amount { get; set; }
        public int? LeaveAllocated { get; set; }
        public int? Total_Working_Days { get; set; }
        public int? Paid_Leave { get; set; }
        public int? UnPaid_Leave { get; set; }
        public int? Total_Days_Worked { get; set; }
        public decimal? ESIC { get; set; }
        public int? Remaining_Leave { get; set; }

        public decimal? Calculated_Salary { get; set; }
        public decimal? Loan { get; set; }
        public decimal? Cash_Pay { get; set; }
        public decimal? Account_Pay { get; set; }
        public decimal? Remaining_Amount { get; set; }
        public string Remarks { get; set; }
        public string CF_referenceId { get; set; }
        public string CF_message { get; set; }
        public bool Active_Bank_Account { get; set; }
        public string IsDraft { get; set; }
    }


    public class AJAX_Pay_Salary
    {
        public AJAX_Pay_Salary()
        {
        }
        public string Month_Year { get; set; }
        public int UserId { get; set; }
        public decimal? Cash_Pay { get; set; }
        public decimal? Account_Pay { get; set; }
        public decimal? ESIC { get; set; }
        public string Remarks { get; set; }
    }


}