﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class RouteReqModel
    {
        public string RequestType { get; set; }
        public string Url { get; set; }
    }
}