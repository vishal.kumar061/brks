﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class Responce_Auth_Model
    {
        public string AccessToken { get; set; }
        public bool IsAdmin { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public int Id { get; set; }
    }
}