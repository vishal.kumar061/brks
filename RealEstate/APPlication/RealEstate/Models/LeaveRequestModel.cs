﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{

    public class LeaveRequestModel
    {
        public LeaveRequestModel()
        {
        }
        public int Id { get; set; }
        public int UserAttendenceId { get; set; }
        public string Name { get; internal set; }
        public string LeaveRequestedOn { get; internal set; }
        public string Reason { get; internal set; }
    }
}