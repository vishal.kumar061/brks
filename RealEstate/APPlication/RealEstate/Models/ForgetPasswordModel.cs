﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class ForgetPasswordModel
    {
        [DisplayName("Name")]
        [Required(ErrorMessage = "Required")]
        public string Email { get; set; }
    }
}