﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class PropertyIndexModel
    {
        public PropertyIndexModel()
        {
            ProjectProperty = new List<Property>();
            ResellerProperty = new List<Property>();
            Project = new List<MA_Project>();
        }
        public IList<Property> ProjectProperty { get; set; }
        public IList<Property> ResellerProperty { get; set; }
        public IList<MA_Project> Project { get; set; }
        public bool IsAdmin { get; set; }
    }
}