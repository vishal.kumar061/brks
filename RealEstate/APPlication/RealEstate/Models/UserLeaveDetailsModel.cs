﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class UserLeaveDetailsModel
    {
        public UserLeaveDetailsModel()
        {
            Leave_Allocated = 0;
            Leave_Taken = 0;
            Worked_On_Holiday = 0;
            Remaining_Leave = 0;
        }
        [DisplayName("Annual Leave Allocated")]
        public int? Leave_Allocated { get; set; }
        [DisplayName("Leave Taken")]
        public int? Leave_Taken { get; set; }
        [DisplayName("Worked On Holiday")]
        public int? Worked_On_Holiday { get; set; }
        [DisplayName("Remaining Leave")]
        public int? Remaining_Leave { get; set; }
    }
}