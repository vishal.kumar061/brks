﻿using RealEstate.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models
{
    public class AttendenceModel
    {
        public AttendenceModel()
        {
            Users = new List<MA_User>();
        }
        public int total_employee { get; set; }
        public int present { get; set; }
        public int absent { get; set; }
        public int leave_request { get; set; }


        public string month_year { get; set; }
        public int total_days { get; set; }
        public IList<MA_User> Users { get; set; }
    }

}