﻿using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace RealEstate.Config
{
    public class AuthenticateAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        private const string Token = "access_token";
        private readonly IActionLog_Service ActionLog_Service;
        private readonly IUserToken_Service UserToken_Service;
        public AuthenticateAttribute()
        {
            ActionLog_Service = new ActionLog_Service();
            UserToken_Service = new UserToken_Service();
        }
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //  Get API key provider
            if (filterContext.Request.Headers.Contains(Token))
            {
                var tokenValue = Regex.Replace(filterContext.Request.Headers.GetValues(Token).First(), "Bearer", "", RegexOptions.IgnoreCase).Trim();

                MA_ActionLog log = new MA_ActionLog()
                {
                    //Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                    //Action = filterContext.ActionDescriptor.ActionName,
                    IP = "NA",
                    CreatedOn = DateTime.Now,
                    UserToken = tokenValue
                };

                if (UserToken_Service.ValidateToken(tokenValue))
                    ActionLog_Service.Add(log);
                else
                    filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Unauthorized Request" };
            }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Unauthorized Request" };
            }
            base.OnActionExecuting(filterContext);
        }
    }
}