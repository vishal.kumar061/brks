﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace RealEstate.Config
{
    public static class SqlHelper
    {
        static FieldInfo rowsCopiedField = null;
        /// <summary>
        /// Gets the rows copied from the specified SqlBulkCopy object
        /// </summary>
        /// <param name="bulkCopy">The bulk copy.</param>
        /// <returns></returns>
        public static int GetRowsCopied(SqlBulkCopy bulkCopy)
        {
            if (rowsCopiedField == null)
            {
                rowsCopiedField = typeof(SqlBulkCopy).GetField("_rowsCopied", BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance);
            }

            return (int)rowsCopiedField.GetValue(bulkCopy);
        }
        public static string Script_CreateTABLE(string table_name, DataTable table)
        {
            string sqlsc;
            sqlsc = "CREATE TABLE " + table_name + "(";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                string columnType = table.Columns[i].DataType.ToString();
                switch (columnType)
                {
                    case "System.Int32":
                        sqlsc += " int ";
                        break;
                    case "System.Int64":
                        sqlsc += " bigint ";
                        break;
                    case "System.Int16":
                        sqlsc += " smallint";
                        break;
                    case "System.Byte":
                        sqlsc += " tinyint";
                        break;
                    case "System.Decimal":
                        sqlsc += " decimal ";
                        break;
                    case "System.DateTime":
                        sqlsc += " datetime ";
                        break;
                    case "System.String":
                    default:
                        sqlsc += string.Format(" nvarchar({0}) ", table.Columns[i].MaxLength == -1 ? "max" : table.Columns[i].MaxLength.ToString());
                        break;
                }
                if (table.Columns[i].AutoIncrement)
                    sqlsc += " IDENTITY(" + table.Columns[i].AutoIncrementSeed.ToString() + "," + table.Columns[i].AutoIncrementStep.ToString() + ") ";
                if (!table.Columns[i].AllowDBNull)
                    sqlsc += " NOT NULL ";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + "\n)";
        }
        public static string GetConnectionstring(string Server_Name, string Database_Name, string UserId, string Password)
        {
            string con = string.Empty;
            if (!string.IsNullOrEmpty(UserId))
            {
                con = "data source=" + Server_Name + ";initial catalog=" + Database_Name + ";persist security info=True;user id=" + UserId + ";password=" + Password + ";MultipleActiveResultSets=True;App=EntityFramework";
            }
            else
            {
                con = "data source=" + Server_Name + ";initial catalog=" + Database_Name + ";integrated security=True;multipleactiveresultsets=True;application name=EntityFramework";
            }
            return con;
        }
        public static string Script_AlterColumn(string table_name, string column_name, string data_type)
        {
            return "ALTER TABLE [dbo]." + table_name + " ALTER COLUMN " + column_name + "  " + data_type + ";";
        }

        public static string Script_CopyData(string dest_table_name, string[] dest_column_name, string source_table_name, string[] source_column_name)
        {
            return "INSERT INTO [dbo]." + dest_table_name + " (" + string.Join(",", dest_column_name) + ") \n SELECT " + string.Join(",", source_column_name) + " FROM  [dbo]." + source_table_name + "";
        }
        public static string Script_DropTable(string table_name)
        {
            return "DROP TABLE [dbo]." + table_name + "";
        }


        public static string GetDataType(string data_type)
        {
            switch (data_type)
            {
                case "nvarchar":
                    return "NVARCHAR(MAX)";
                default:
                    return data_type;
            }

        }


        public static bool TestConnection(string Connectionstring)
        {
            string commandText = "select * from sys.objects";
            using (SqlConnection conn = new SqlConnection(Connectionstring))
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return true;
        }

        public static string Script_GetUser_Db_Access(string login_db_user_name)
        {

            return "DECLARE @DB_USers TABLE(" + "\n" +
                   "DBName SYSNAME" + "\n" +
                   ", UserName SYSNAME" + "\n" +
                   ", LoginType SYSNAME" + "\n" +
                   ", AssociatedRole VARCHAR(max)" + "\n" +
                   ", create_date DATETIME" + "\n" +
                   ", modify_date DATETIME" + "\n" +
                   ")" + "\n" +

                   "IF ('"+ login_db_user_name + "'=(SELECT UserName from(" + "\n" +
                   "SELECT [name] as username,type_desc,is_disabled" + "\n" +
                   "FROM     master.sys.server_principals" + "\n" +
                   "WHERE    IS_SRVROLEMEMBER ('sysadmin',name) = 1 AND [name]='" + login_db_user_name + "')  as a))" + "\n" +
                   "BEGIN" + "\n" +
                   "SELECT NAME AS dbname,'" + login_db_user_name + "' AS username,'SQL_USER' AS logintype,NULL AS create_date,NULL AS modify_date,'db_owner' AS Permissions_user  FROM SYS.DATABASES" + "\n" +
                   "END" + "\n" +
                   "ELSE" + "\n" +
                   "BEGIN" + "\n" +

"INSERT @DB_USers" + "\n" +

"EXEC sp_MSforeachdb" + "\n" +
"'use [?]" + "\n" +
"SELECT '' ? '' AS DB_Name," + "\n" +
"case prin.name when ''dbo'' then prin.name + ''('' + (select SUSER_SNAME(owner_sid) from master.sys.databases where name = ''?'') + '')'' else prin.name end AS UserName," + "\n" +
   "prin.type_desc AS LoginType," + "\n" +
"isnull(USER_NAME(mem.role_principal_id), '''') AS AssociatedRole, create_date, modify_date" + "\n" +
"FROM sys.database_principals prin" + "\n" +
"LEFT OUTER JOIN sys.database_role_members mem ON prin.principal_id = mem.member_principal_id" + "\n" +
"WHERE prin.sid IS NOT NULL and prin.sid NOT IN(0x00) and" + "\n" +
"prin.is_fixed_role <> 1 AND prin.name NOT LIKE ''##%'''" + "\n" +

"SELECT dbname" + "\n" +
       ", username" + "\n" +
       ", logintype" + "\n" +
       ", create_date" + "\n" +
       ", modify_date" + "\n" +
       ", STUFF((" + "\n" +
                    "SELECT ',' + CONVERT(VARCHAR(500), associatedrole)" + "\n" +
                    "FROM @DB_USers user2" + "\n" +
                    "WHERE user1.DBName = user2.DBName" + "\n" +
                           "AND user1.UserName = user2.UserName" + "\n" +
                    "FOR XML PATH('')" + "\n" +
                    "), 1, 1, '') AS Permissions_user" + "\n" +
"FROM @DB_USers user1" + "\n" +
"WHERE username <> 'public' AND username ='" + login_db_user_name + "'" + "\n" +
"GROUP BY dbname" + "\n" +
       ",username" + "\n" +
       ",logintype" + "\n" +
       ",create_date" + "\n" +
       ",modify_date" + "\n" +
"ORDER BY DBName" + "\n" +
       ", username" + "\n" +
       "END";

        }
    }
}