﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using Microsoft.VisualBasic.FileIO;

namespace RealEstate.Config
{
    public static class Extention
    {
        public static object ReadCsvFile(string file_url)
        {
            DataTable dtCsv = new DataTable();
            string[] header = null;
            using (TextFieldParser parser = new TextFieldParser(file_url))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                bool firstLine = true;

                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    if (firstLine)
                    {
                        for (int k = 0; k < fields.Count(); k++)
                        {
                            dtCsv.Columns.Add(fields[k]);
                        }
                        header = fields;
                        firstLine = false;
                        continue;
                    }
                    else
                    {
                        DataRow dr = dtCsv.NewRow();
                        for (int k = 0; k < fields.Count(); k++)
                        {
                            dr[k] = fields[k].ToString();
                        }
                        dtCsv.Rows.Add(dr);
                    }
                }
            }



            var obj = new { Datatable = dtCsv, Header = header.ToArray() };
            return obj;
        }

        public static List<T> ToListOf<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);

                data.Add(item);
            }
            return data;
        }

        public static T ToModelOf<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);

                data.Add(item);
            }
            return data.FirstOrDefault();
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public static DataTable ToDataTable<T>(this T data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            //foreach (T item in data)
            //{
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = props[i].GetValue(data);
            }
            table.Rows.Add(values);
            //}
            return table;
        }


        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {

                    //if (typeof(IEnumerable).IsAssignableFrom(pro.PropertyType))

                    if (pro.Name == column.ColumnName && pro.PropertyType.IsGenericType && pro.PropertyType.Namespace.ToLower().Trim() == "system.collections.generic")
                        pro.SetValue(obj, dr[column.ColumnName] == DBNull.Value ? null : dr[column.ColumnName].ToString().Split(','), null);
                    else if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName] == DBNull.Value ? null : dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}