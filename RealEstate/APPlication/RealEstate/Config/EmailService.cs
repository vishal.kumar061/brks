﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace RealEstate.Config
{
    public interface IEmailService
    {
        DataResult<string> email(string[] ToEmail, string body, string subject, string[] ccEmail = null);
    }
    public class EmailService : IEmailService
    {
        public DataResult<string> email(string[] ToEmail, string body, string subject, string[] ccEmail = null)
        {
            var result = new DataResult<string>();
            try
            {
                string smtpServer = ConfigurationManager.AppSettings["smtpServer"].ToString();
                string smtpUser = ConfigurationManager.AppSettings["smtpUser"].ToString();
                string smtpPwd = ConfigurationManager.AppSettings["smtpPwd"].ToString();
                string smtpPort = ConfigurationManager.AppSettings["smtpPort"].ToString();
                string displayName = ConfigurationManager.AppSettings["displayName"].ToString();

                MailMessage Msg = new MailMessage();
                Msg.From = new MailAddress(smtpUser, displayName);

                if (ToEmail != null)
                    ToEmail.ForEach(a => Msg.To.Add(a));
                if (ccEmail != null)
                    ccEmail.ForEach(a => Msg.CC.Add(a));
                Msg.Subject = subject;
                Msg.IsBodyHtml = true;
                Msg.Body = body;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = smtpServer;
                smtp.Port = int.Parse(smtpPort);
                smtp.Credentials = new System.Net.NetworkCredential(smtpUser, smtpPwd);
                smtp.EnableSsl = true;
                smtp.Send(Msg);
                result.Data = "Email sent";
                result.StatusCode = APIStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.ErrorSendingMail;
            }
            return result;
        }
    }
}