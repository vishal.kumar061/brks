﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace RealEstate.Config
{
    public static class AccessControl
    {
        public static List<string> UserAccessControlUrl()
        {
            return new List<string>()
            {
                "/user/addedituser/{id}"
            };
        }
    }
}