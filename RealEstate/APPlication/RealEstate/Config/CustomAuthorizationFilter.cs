﻿using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RealEstate.Config
{

    public class CustomAuthorizationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                string Url = filterContext.HttpContext.Request.RawUrl;
                //string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
                //string myIP = Dns.GetHostEntry(hostName).AddressList[0].ToString();

                var user = (Responce_Auth_Model)filterContext.HttpContext.Session["user"];
                //var model = new MA_ActionLog()
                //{
                //    Url = filterContext.HttpContext.Request.RawUrl,
                //    RequestType = filterContext.HttpContext.Request.RequestType,
                //    CreatedOn = DateTime.Now,
                //    UserToken = user.AccessToken,
                //    IP = hostName + "," + myIP
                //};

                //new ActionLog_Service().Add(model);

                if (user != null && !(user.RoleName.ToLower().Contains("admin") || user.RoleName.ToLower().Contains("manager")))
                {
                    var Access_url = new List<string>();
                    var UserRoles = new MA_User_Service().Get(user.Id).UserRoles;
                    foreach (var ur in UserRoles)
                    {
                        foreach (var url in ur.MA_Role.RoleUrlAccesses)
                        {
                            Access_url.Add(url.Access_Url);
                        }
                    }
                    Access_url = Access_url.Select(x => x.Replace("{id}", user.Id.ToString())).ToList();

                    if (!Access_url.Any(x => x.ToLower().Trim().Equals(Url.ToLower().Trim())))
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Error" }, { "action", "Unauthorized" } });
                    }
                }
                else if (user == null)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "controller", "Account" },
                    { "action", "Logout" }
                });
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Account" }, { "action", "Logout" } });
                base.OnActionExecuting(filterContext);
            }
        }

    }
}