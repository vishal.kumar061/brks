﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace RealEstate.Config
{
    public static class Cashfree_Payouts_Config
    {
        public static string API_BaseAddress = ConfigurationManager.AppSettings["API_BaseAddress"];
        public static string Client_Id = ConfigurationManager.AppSettings["Client_Id"];
        public static string Client_Secret = ConfigurationManager.AppSettings["Client_Secret"];


        public static DataResult<string> GetToken()
        {
            var result = new DataResult<string>();
            try
            {
                using (var client = new HttpClient())
                {
                    object args = null;
                    client.BaseAddress = new Uri(API_BaseAddress);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("X-Client-Id", Client_Id);
                    client.DefaultRequestHeaders.Add("X-Client-Secret", Client_Secret);
                    var response = client.PostAsJsonAsync("payout/v1/authorize", args).Result;
                    object returnValue = response.Content.ReadAsAsync<object>().Result;
                    string src = JsonConvert.SerializeObject(returnValue);
                    dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);
                    // return data.data.token;
                    result.status = data.status;
                    result.message = data.message;
                    result.subCode = data.subCode;
                    if (data.subCode == "200")
                        result.Data = data.data.token;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.message = ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message;
                return result;
            }
        }
        public static DataResult<string> VerifyToken(string token)
        {
            var result = new DataResult<string>();
            using (var client = new HttpClient())
            {
                object args = null;
                client.BaseAddress = new Uri(API_BaseAddress);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                var response = client.PostAsJsonAsync("payout/v1/verifyToken", args).Result;
                object returnValue = response.Content.ReadAsAsync<object>().Result;
                string src = JsonConvert.SerializeObject(returnValue);
                dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

                result.status = data.status;
                result.message = data.message;
                result.subCode = data.subCode;
                return result;
            }

            //            {
            //                "status":"SUCCESS", 
            //"message":"Token is valid", 
            //"subCode":"200"}
        }
        public static DataResult<string> AddBeneficiary(string token, string beneId, string name, string email, string phone, string bankAccount, string ifsc, string address1, string address2 = "", string city = "", string state = "", string pincode = "", string vpa = "", string cardNo = "")
        {
            var result = new DataResult<string>();
            using (var client = new HttpClient())
            {
                object args = new
                {
                    beneId = beneId,
                    name = name,
                    email = email,
                    phone = phone,
                    bankAccount = bankAccount,
                    ifsc = ifsc,
                    vpa = vpa,
                    cardNo = cardNo,
                    address1 = address1,
                    address2 = address2,
                    city = city,
                    state = state,
                    pincode = pincode,
                };
                client.BaseAddress = new Uri(API_BaseAddress);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                var response = client.PostAsJsonAsync("payout/v1/addBeneficiary", args).Result;
                object returnValue = response.Content.ReadAsAsync<object>().Result;
                string src = JsonConvert.SerializeObject(returnValue);
                dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

                result.status = data.status;
                result.message = data.message;
                result.subCode = data.subCode;
                return result;
            }

            //            {
            //                "status":"SUCCESS",
            //"subCode":"200",
            //"message":"Beneficiary added successfully"
            //}
        }
        public static string GetBeneficiaryDetails(string token)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(API_BaseAddress);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = client.GetAsync("payout/v1/getBeneficiary/1").Result;  // Blocking call!  

            object returnValue = response.Content.ReadAsAsync<object>().Result;
            string src = JsonConvert.SerializeObject(returnValue);
            dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);
            return data.data;

            //            {
            //                "status":"SUCCESS", 
            //"subCode":"200", 
            //"message":"Details of beneficiary", 
            //"data": {
            //                    "beneId":"JOHN18011", 
            //        "name":"John",
            //        "groupName":"DEFAULT", 
            //        "email":"johndoe@cashfree.com", 
            //        "phone":"9876543210", 
            //        "address1":"ABCavenue", 
            //        "address2":"", 
            //        "city":"Bangalore", 
            //        "state":"Karnataka", 
            //        "pincode":"0", 
            //        "bankAccount":"00001111222233", 
            //        "ifsc":"HDFC0000001", "status":"VERIFIED"}
            //            }
        }
        public static DataResult<string> RemoveBeneficiary(string token, string beneId)
        {
            var result = new DataResult<string>();
            using (var client = new HttpClient())
            {
                object args = new { beneId = beneId };
                client.BaseAddress = new Uri(API_BaseAddress);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                var response = client.PostAsJsonAsync("payout/v1/removeBeneficiary", args).Result;

                object returnValue = response.Content.ReadAsAsync<object>().Result;
                string src = JsonConvert.SerializeObject(returnValue);
                dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

                result.status = data.status;
                result.message = data.message;
                result.subCode = data.subCode;
                return result;
            }
            //            {
            //                "status":"SUCCESS", 
            //"subCode":"200", 
            //"message":"Beneficiary removed"}
        }
        public static DataResult<CF_Payouts_GetBalanceModel> GetBalance(string token)
        {
            var result = new DataResult<CF_Payouts_GetBalanceModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(API_BaseAddress);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = client.GetAsync("payout/v1/getBalance").Result;  // Blocking call!  

            object returnValue = response.Content.ReadAsAsync<object>().Result;
            string src = JsonConvert.SerializeObject(returnValue);
            dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

            result.status = data.status;
            result.message = data.message;
            result.subCode = data.subCode;
            if (data.subCode == "200")
                result.Data = new CF_Payouts_GetBalanceModel
                {
                    balance = data.data.balance,
                    availableBalance = data.data.availableBalance
                };
            return result;

            //            {
            //                "status":"SUCCESS", 
            //"subCode":"200", 
            //"message":"Ledger balance for the account", 
            //"data": { "balance":"214735.50", "availableBalance":"173980.50"}
            //            }
        }
        public static DataResult<string> SelfWithdrawal(string token, string withdrawalId, decimal amount)
        {
            var result = new DataResult<string>();
            using (var client = new HttpClient())
            {
                object args = new
                {
                    withdrawalId = withdrawalId,
                    amount = amount
                };
                client.BaseAddress = new Uri(API_BaseAddress);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                var response = client.PostAsJsonAsync("payout/v1/selfWithdrawal", args).Result;
                object returnValue = response.Content.ReadAsAsync<object>().Result;
                string src = JsonConvert.SerializeObject(returnValue);
                dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

                result.status = data.status;
                result.message = data.message;
                result.subCode = data.subCode;
                return result;
            }
            //            {
            //                "status": "SUCCESS",
            //"message": "Request submitted successfully. Withdrawal Id : W55",
            //"statusCode": "200"}
        }

        public static DataResult<CF_Payouts_RequestTransferModel> RequestTransfer(string token, string beneId, decimal amount, string transferId)
        {
            var result = new DataResult<CF_Payouts_RequestTransferModel>();
            using (var client = new HttpClient())
            {
                object args = new
                {
                    beneId = beneId,
                    amount = amount,
                    transferId = transferId,
                };
                client.BaseAddress = new Uri(API_BaseAddress);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                var response = client.PostAsJsonAsync("payout/v1/requestTransfer", args).Result;
                object returnValue = response.Content.ReadAsAsync<object>().Result;
                string src = JsonConvert.SerializeObject(returnValue);
                dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

                result.status = data.status;
                result.message = data.message;
                result.subCode = data.subCode;
                if (data.subCode == "200")
                    result.Data = new CF_Payouts_RequestTransferModel
                    {
                        referenceId = data.data.referenceId,
                        utr = data.data.utr,
                        acknowledged = data.data.acknowledged
                    };
                return result;
            }

            //            {
            //                "status":"SUCCESS", 
            //"subCode":"200", 
            //"message":"Transfer completed successfully", 
            //"data": {
            //                    "referenceId":"10023",
            //        "utr":"P16111765023806",
            //        "acknowledged": 1}
            //            }
        }
        public static DataResult<CF_Payouts_GetTransferStatusModel> GetTransferStatus(string token, string transferId, string referenceId = "")
        {
            var result = new DataResult<CF_Payouts_GetTransferStatusModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(API_BaseAddress);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            //HttpResponseMessage response = client.GetAsync("payout/v1/getTransferStatus?referenceId=" + referenceId + "&transferId=" + transferId).Result;  // Blocking call!  

            HttpResponseMessage response = client.GetAsync("payout/v1/getTransferStatus?transferId=" + transferId).Result;

            object returnValue = response.Content.ReadAsAsync<object>().Result;
            string src = JsonConvert.SerializeObject(returnValue);
            dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

            result.status = data.status;
            result.message = data.message;
            result.subCode = data.subCode;
            if (data.subCode == "200")
                result.Data = new CF_Payouts_GetTransferStatusModel
                {
                    referenceId = data.data.transfer.referenceId,
                    bankAccount = data.data.transfer.bankAccount,
                    beneId = data.data.transfer.beneId,
                    amount = data.data.transfer.amount,
                    status = data.data.transfer.status,
                    utr = data.data.transfer.utr,
                    addedOn = data.data.transfer.addedOn,
                    processedOn = data.data.transfer.processedOn,
                    acknowledged = data.data.transfer.acknowledged,
                };
            return result;

            //           {
            //               "status": "SUCCESS", 
            //"subCode": "200", 
            //"message": "Details of transfer with transferId 159381033b123", 
            //"data": {
            //                   "transfer": 
            //				{
            //                       "referenceId": 17073, 
            //				"bankAccount": "026291800001191", 
            //				"beneId": "ABCD_123", 
            //				"amount": "20.00", 
            //				"status": "SUCCESS", 
            //				"utr": "1387420170430008800069857", 
            //				"addedOn": "2017­-01­-07 20:09:59", 
            //				"processedOn": "2017­-01­-07 20:10:05", 
            //				"acknowledged": 1 }
            //               }
            //           }
        }

        public static DataResult<CF_Payouts_BankVerificationModel> BankVerification(string token, string name, string phone, string bankAccount, string ifsc)
        {
            var result = new DataResult<CF_Payouts_BankVerificationModel>();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(API_BaseAddress);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = client.GetAsync("payout/v1/validation/bankDetails?name=" + name + "&phone=" + phone + "&bankAccount=" + bankAccount + "&ifsc=" + ifsc).Result;

            object returnValue = response.Content.ReadAsAsync<object>().Result;
            string src = JsonConvert.SerializeObject(returnValue);
            dynamic data = Newtonsoft.Json.Linq.JObject.Parse(src);

            result.status = data.status;
            result.message = data.message;
            result.subCode = data.subCode;
            if (data.subCode == "200")
                result.Data = new CF_Payouts_BankVerificationModel
                {
                    nameAtBank = data.data.nameAtBank,
                    accountExists = data.data.accountExists,
                    amountDeposited = data.data.amountDeposited,
                    refId = data.data.refId,
                };
            return result;


            //           {
            //               "status": "SUCCESS", 
            //"subCode": "200", 
            //"message": "Amount Deposited Successfully", 
            //"data": {
            //                   "nameAtBank": "John Doe", 
            //         "accountExists": "YES", 
            //         "amountDeposited": "1.28", 
            //         "refId": "123456"
            //        }
            //           }
        }
    }
}


public class CF_Payouts_GetBalanceModel
{
    public string balance { get; set; }
    public string availableBalance { get; set; }
}
public class CF_Payouts_RequestTransferModel
{
    public string referenceId { get; set; }
    public string utr { get; set; }
    public string acknowledged { get; set; }
}
public class CF_Payouts_GetTransferStatusModel
{
    public string referenceId { get; set; }
    public string bankAccount { get; set; }
    public string beneId { get; set; }
    public string amount { get; set; }
    public string status { get; set; }
    public string utr { get; set; }
    public string addedOn { get; set; }
    public string processedOn { get; set; }
    public string acknowledged { get; set; }
}
public class CF_Payouts_BankVerificationModel
{
    public string nameAtBank { get; set; }
    public string accountExists { get; set; }
    public string amountDeposited { get; set; }
    public string refId { get; set; }
}
