﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Controllers
{
    [CustomAuthorizationFilter]
    public class SalaryController : Controller
    {
        private readonly IMA_User_Service MA_User_Service;
        private readonly IUtility_Service Utility_Service;
        private readonly IMA_Project_Service MA_Project_Service;

        public SalaryController()
        {
            MA_User_Service = new MA_User_Service();
            Utility_Service = new Utility_Service();
            MA_Project_Service = new MA_Project_Service();
        }
        // GET: Salary
        public ActionResult Index(string Month_Year)
        {
            if (string.IsNullOrEmpty(Month_Year))
            {
                CultureInfo ci = new CultureInfo("en-US");
                Month_Year = DateTime.Now.ToString("MMM yyyy", ci);
            }

            var result = MA_User_Service.GetSalary(Month_Year);

            var model = new UserSalaryModel
            {
                Month_Year = Month_Year,
                UserSalary = result.ToList(),
                Total_Amount = (decimal)result.ToList().Sum(x => x.Remaining_Amount)
            };
            return View(model);
        }
        public ActionResult Generate_Salary(string Month_Year)
        {
            if (string.IsNullOrEmpty(Month_Year))
            {
                CultureInfo ci = new CultureInfo("en-US");
                Month_Year = DateTime.Now.ToString("MMM yyyy", ci);
            }
            var result = MA_User_Service.GenerateSalary(Month_Year);
            return RedirectToAction("Index",new { Month_Year });
        }

        [HttpPost]
        public ActionResult Index(UserSalaryModel model)
        {
            var UserList = MA_User_Service.Get().ToList();
            return RedirectToAction("Index", new { Month_Year = model.Month_Year, Regenerate = true });
        }
        [HttpPost]
        public JsonResult AJAX_Pay_Salary(List<AJAX_Pay_Salary> model)
        {
            var result = MA_User_Service.AJAX_Pay_Salary(model);
            return Json(result);
        }
        [HttpPost]
        public JsonResult AJAX_View_Details(int UserId, string Month_year)
        {
            var result = MA_User_Service.AJAX_View_Details(UserId, Month_year);
            return Json(result);
        }

        [HttpPost]
        public JsonResult AJAX_RefreshTransferStatus(int UserSalaryId)
        {
            var result = MA_User_Service.RefreshTransferStatus(UserSalaryId);
            return Json(result);
        }








    }
}