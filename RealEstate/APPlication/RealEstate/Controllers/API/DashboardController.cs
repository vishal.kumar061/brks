﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RealEstate.Controllers.API
{
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        private readonly IMA_User_Service MA_User_Service;
        //private readonly IProperty_Service Property_Service;
        private readonly IOTP_Service OTP_Service;
        //private readonly IMA_Project_Service MA_Project_Service;
        //private readonly IMA_ConfigFilter_Service MA_ConfigFilter_Service;


        public DashboardController()
        {
            MA_User_Service = new MA_User_Service();
            //Property_Service = new Property_Service();
            OTP_Service = new OTP_Service();
            //MA_Project_Service = new MA_Project_Service();
            //MA_ConfigFilter_Service = new MA_ConfigFilter_Service();
        }
        [HttpGet, Route("otp")]
        public DataResult<string> get_otp(string Mob)
        {
            var model = new OTP_GetModel { Mob = Mob };
            return OTP_Service.get_otp(model);
        }
        [HttpPost, Route("otp")]
        public DataResult<Responce_Auth_Model> validate_otp(OTP_ValidateModel model)
        {
            return OTP_Service.validate_otp(model);
        }

        [HttpPost, Route("signup")]
        public DataResult<string> signup(SignUpModel model)
        {
            return MA_User_Service.SignUp(model);
        }
        [HttpPost, Route("login")]
        public DataResult<Responce_Auth_Model> login(Req_Auth_Model model)
        {
            return MA_User_Service.AccessToken(model);
        }

        [HttpPost, Route("forgetpassword")]
        public DataResult<string> forgetpassword(ForgetPasswordModel model)
        {
            return MA_User_Service.ForgetPassword(model);
        }

        //[HttpGet, Route("project")]
        //public DataResult<IList<Object>> project()
        //{
        //    return MA_Project_Service.GetProject();
        //}

        //[HttpGet, Route("property")]
        //public DataResult<IList<PropertyModel>> property(int ProjectId)
        //{
        //    return Property_Service.GetByProjectId(ProjectId);
        //}

        //[HttpGet, Route("project_property")]
        //public DataResult<IList<PropertyModel>> property()
        //{
        //    return Property_Service.Get(IsProject: true);
        //}
        //[HttpGet, Route("reseller_property")]
        //public DataResult<IList<PropertyModel>> reseller_property()
        //{
        //    return Property_Service.Get(IsReseller: true);
        //}

        //[HttpGet, Route("property_detail")]
        //public DataResult<PropertyModel> property_detail(int PropertyId)
        //{
        //    return Property_Service.GetByPropertyId(PropertyId);
        //}

        //[HttpGet, Route("property_rent")]
        //public DataResult<IList<PropertyModel>> property_rent()
        //{
        //    return Property_Service.GetProperty_rent();
        //}

        //[HttpGet, Route("property_sale")]
        //public DataResult<IList<PropertyModel>> property_sale()
        //{
        //    return Property_Service.GetProperty_sale();
        //}


        //[HttpPost, Route("add_property")]
        //public DataResult<string> add_property([FromBody]PropertyInfoModel model)
        //{
        //    return Property_Service.AddEditProperty(model);
        //}


        //[HttpGet, Route("config")]
        //public DataResult<IList<Object>> config()
        //{
        //    return MA_ConfigFilter_Service.GetAll();
        //}








        //[HttpPost, Route("search")]
        //public DataResult<IList<PropertyModel>> search([FromBody]FilterModel model)
        //{
        //    return Property_Service.search(model);
        //}

    }
}
