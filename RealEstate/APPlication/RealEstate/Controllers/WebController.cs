﻿using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Controllers
{
    public class WebController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Properties()
        {
            return View();
        }

        public ActionResult Property()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

    }
}