﻿using RealEstate.Config;
using RealEstate.Database.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;
using RealEstate.Database.Entity;
using RealEstate.Models.API.Account;
using System.Text.RegularExpressions;
using RealEstate.Models;

namespace RealEstate.Controllers
{
    public class PropertyController : Controller
    {
        private readonly ICommon_Service Common_Service;
        private readonly IProperty_Service Property_Service;
        private readonly IUserToken_Service UserToken_Service;
        private readonly IMA_Project_Service MA_Project_Service;
        public PropertyController()
        {
            Common_Service = new Common_Service();
            Property_Service = new Property_Service();
            UserToken_Service = new UserToken_Service();
            MA_Project_Service = new MA_Project_Service();
        }

        public ActionResult Index()
        {
            var model = new PropertyIndexModel();
            model.IsAdmin = (Session["user"] as Responce_Auth_Model).IsAdmin;
            if (model.IsAdmin)
            {
                model.ResellerProperty = Property_Service.GetResellerProperty().ToList();
                model.ProjectProperty = Property_Service.GetProjectProperty().ToList();
                model.Project = MA_Project_Service.Get().ToList();
                return View(model);
            }
            else
            {
                var entity = Property_Service.GetbyUserId(UserToken_Service.Get(Session["token"].ToString()).UserId).ToList();
                return View(model);
            }
        }

        public ActionResult AddEditProperty(int id = 0, bool IsProjectProperty = true)
        {
            var model = new PropertyInfoModel();
          
            if (id != 0)
            {
                var entity = Property_Service.Get(id);
                model = entity.ToDataTable().ToModelOf<PropertyInfoModel>();
            }
            model = Common_Service.FillDropdown(model);
            model.IsprojectProperty = IsProjectProperty;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditProperty(PropertyInfoModel model)
        {
            model = Common_Service.FillDropdown(model);
            if (ModelState.IsValid)
            {
                model.IsAdmin = (Session["user"] as Responce_Auth_Model).IsAdmin;
                foreach (HttpPostedFileBase file in model.Files)
                {
                    if (file != null)
                    {
                        var InputFileName = Regex.Replace(Guid.NewGuid().ToString().Substring(0, 7), @"[^0-9a-zA-Z]+", "") + "_" + Path.GetFileName(file.FileName);
                        model.modified_file_name.Add(InputFileName);
                        var ServerSavePath = Path.Combine(Server.MapPath("~//Uploads//PropertyImages//") + InputFileName);
                        file.SaveAs(ServerSavePath);
                    }
                }
                model.CreatedBy = UserToken_Service.Get(Session["token"].ToString()).UserId;
                var result = Property_Service.AddEditProperty(model);
                if (result.Success)
                    return RedirectToAction("Index");
                else
                    return View(model);
            }
            return View(model);
        }

        public ActionResult DeleteProperty(int id)
        {
            var result = Property_Service.DeleteProperty(id);
            return RedirectToAction("Index");

        }
        public ActionResult ApproveRejectProperty(int id)
        {
            var result = Property_Service.ApproveRejectProperty(id);
            return RedirectToAction("Index");

        }
        public ActionResult ViewProperty(int id = 0)
        {
            return View(Property_Service.Get(id));
        }
    }
}
