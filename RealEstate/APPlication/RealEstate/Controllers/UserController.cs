﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace RealEstate.Controllers
{
    [CustomAuthorizationFilter]
    public class UserController : Controller
    {
        private readonly IMA_User_Service MA_User_Service;
        private readonly IUtility_Service Utility_Service;
        private readonly IMA_Project_Service MA_Project_Service;

        public UserController()
        {
            MA_User_Service = new MA_User_Service();
            Utility_Service = new Utility_Service();
            MA_Project_Service = new MA_Project_Service();
        }
        public ActionResult Index()
        {
            var entity = MA_User_Service.GetAll();
            return View(entity);
        }
        public ActionResult AddEditUser(int id = 0)
        {
            var model = new UserModel();
            if (id != 0)
            {
                model = MA_User_Service.GetUser(id).Data;
            }
            model.RoleLst = (from x in MA_User_Service.GetRole() select new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            model.BankLst = (from x in Utility_Service.GetBankList() select new SelectListItem { Text = x.BankName, Value = x.Id.ToString() }).ToList();
            model.ProjectLst = (from x in MA_Project_Service.GetAll() select new SelectListItem { Text = x.ProjectName, Value = x.Id.ToString() }).ToList();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddEditUser(UserModel model)
        {
            var session_user = (Responce_Auth_Model)HttpContext.Session["user"];
            model.IsAdmin = session_user.IsAdmin;
            model.RoleLst = (from x in MA_User_Service.GetRole() select new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            model.BankLst = (from x in Utility_Service.GetBankList() select new SelectListItem { Text = x.BankName, Value = x.Id.ToString() }).ToList();
            model.ProjectLst = (from x in MA_Project_Service.GetAll() select new SelectListItem { Text = x.ProjectName, Value = x.Id.ToString() }).ToList();


            if (ModelState.IsValid)
            {
                var result = MA_User_Service.AddEditUser(model);
                if (result.Success)
                {
                    TempData["Message"] = result.message;
                    if (model.IsAdmin)
                        return RedirectToAction("AddEditUser", new { result.Data.Id });
                    else
                        return RedirectToAction("AddEditUser", new { result.Data.Id });
                }
                else
                {
                    TempData["Message"] = result.message;
                    return View(model);
                }
            }
            return View(model);
        }
        public ActionResult DeleteUser(int id)
        {
            var result = MA_User_Service.DeleteUser(id);
            return RedirectToAction("Index");
        }


        //----------------------insentive----------------
        public ActionResult Insentive()
        {
            var entity = MA_User_Service.GetAll_UserInsentive();
            return View(entity);
        }
        public ActionResult AddEditInsentive(int id = 0)
        {
            var entity = MA_User_Service.Get_UserInsentive(id);
            return View(entity);
        }
        [HttpPost]
        public ActionResult AddEditInsentive(InsentiveModel model)
        {
            if (ModelState.IsValid)
            {
                var entity = MA_User_Service.AddEditInsentive(model);
                return RedirectToAction("Insentive");
            }
            else
            {
                var entity = MA_User_Service.Get_UserInsentive(0);
                return View(entity);
            }
            
        }


    }
}
