﻿using RealEstate.Config;
using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Controllers
{
    [CustomAuthorizationFilter]
    public class LoanController : Controller
    {
        private readonly IMA_Loan_Service MA_Loan_Service;
        private readonly IMA_User_Service MA_User_Service;
        public LoanController()
        {
            MA_Loan_Service = new MA_Loan_Service();
            MA_User_Service = new MA_User_Service();
        }
        public ActionResult Index()
        {
            return View(MA_Loan_Service.GetOverAll());
        }

        public ActionResult AddEditLoan(int id = 0)
        {
            var model = this.GetById(id).Data;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditLoan(LoanModel model)
        {
            if (ModelState.IsValid)
            {
                var result = MA_Loan_Service.AddEditLoan(model);
                if (result.Success)
                    return RedirectToAction("Index");
                else
                    return View(model);
            }
            else
            {
                model = this.GetById(0).Data;
            }
            return View(model);

        }
        public ActionResult DeleteLoan(int id)
        {
            var result = MA_Loan_Service.DeleteLoan(id);
            return RedirectToAction("Index");
        }

        public DataResult<LoanModel> GetById(int id)
        {
            var result = new DataResult<LoanModel>();
            try
            {
                if (id == 0)
                {
                    result.Data = new LoanModel
                    {
                        UserLst = (from y in MA_User_Service.Get() 
                                   where y.UserBankDetails.Count>0 && y.UserBankDetails.First().Is_Active==true 
                                   select new SelectListItem { Text = y.Name, Value = y.Id.ToString() }).ToList()
                    };
                    result.StatusCode = APIStatusCode.OK;
                }
                else
                {
                    var entity = MA_Loan_Service.Get(id);
                    var model = new LoanModel
                    {
                        Installement = entity.Installement,
                        IsActive = entity.IsActive,
                        Loan = entity.Loan,
                        LoanId = entity.Id,
                        UserId = entity.UserId,
                        UserLst = (from y in MA_User_Service.Get()
                                   where y.UserBankDetails.Count > 0 && y.UserBankDetails.First().Is_Active == true
                                   select new SelectListItem { Text = y.Name, Value = y.Id.ToString() }).ToList(),
                        Mode = entity.Mode
                    };
                    result.StatusCode = APIStatusCode.OK;
                    result.Data = model;
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }
    }
}