﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Controllers
{
    [CustomAuthorizationFilter]
    public class LeaveController : Controller
    {
        // GET: Leave
        private readonly IMA_Leave_Service MA_Leave_Service;
        public LeaveController()
        {
            MA_Leave_Service = new MA_Leave_Service();
        }
        public ActionResult Index()
        {
            return View(MA_Leave_Service.GetAll());
        }

        public ActionResult AddEditLeave(int id = 0)
        {
            var model = new MA_Leave();
            if (id != 0)
            {
                model = MA_Leave_Service.Get(id);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditLeave(MA_Leave model)
        {
            if (ModelState.IsValid)
            {
                var result = MA_Leave_Service.AddEditLeave(model);
                if (result.Success)
                    return RedirectToAction("Index");
                else
                    return View(model);
            }
            return View(model);

        }
        public ActionResult DeleteLeave(int id)
        {
            var result = MA_Leave_Service.DeleteLeave(id);
            return RedirectToAction("Index");
        }
    }
}