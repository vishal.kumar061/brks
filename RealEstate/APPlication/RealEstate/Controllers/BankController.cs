﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Controllers
{
    [CustomAuthorizationFilter]
    public class BankController : Controller
    {
        // GET: Leave
        private readonly IMA_Bank_Service MA_Bank_Service;
        public BankController()
        {
            MA_Bank_Service = new MA_Bank_Service();
        }
        public ActionResult Index()
        {
            return View(MA_Bank_Service.GetAll());
        }

        public ActionResult AddEditBank(int id = 0)
        {
            var model = new MA_Bank();
            if (id != 0)
            {
                model = MA_Bank_Service.Get(id);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditBank(MA_Bank model)
        {
            if (ModelState.IsValid)
            {
                var result = MA_Bank_Service.AddEditBank(model);
                if (result.Success)
                    return RedirectToAction("Index");
                else
                    return View(model);
            }
            return View(model);
        }
    }
}