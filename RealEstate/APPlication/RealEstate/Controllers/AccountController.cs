﻿using RealEstate.Database.Service;
using RealEstate.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealEstate.Config;

namespace RealEstate.Controllers
{
    public class AccountController : Controller
    {
        private readonly IMA_User_Service MA_User_Service;

        public AccountController()
        {
            MA_User_Service = new MA_User_Service();
        }
        //------------------Sign Up------------------------
        public ActionResult SignUp()
        {
            var model = new SignUpModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult SignUp(SignUpModel model)
        {
            if (ModelState.IsValid)
            {
                var result = MA_User_Service.SignUp(model);
                if (result.Success)
                {
                    TempData["Message"] = result.Data;
                    return RedirectToAction("Index");
                }
                TempData["Message"] = result.ErrorMessages;
                return RedirectToAction("SignUp");
            }
            return View();
        }
        //------------------Login------------------------
        public ActionResult Index()
        {
            var model = new Req_Auth_Model();
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(Req_Auth_Model m)
        {
            var result = MA_User_Service.AccessToken(m);
            if (result.Success)
            {
                Session["token"] = result.Data.AccessToken;
                Session["user"] = result.Data;
                Session["rolename"] = result.Data.RoleName;

                if (result.Data.RoleName.ToLower().Contains("admin") || result.Data.RoleName.ToLower().Contains("manager"))
                    return RedirectToRoute("UserList");
                else
                    return RedirectToRoute("AddEditUser", new { result.Data.Id });
            }
            TempData["Message"] = result.ErrorMessages;
            return RedirectToAction("Index");
        }
        //------------------Forget Password------------------------
        public ActionResult ForgetPassword()
        {
            var model = new ForgetPasswordModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ForgetPassword(ForgetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var result = MA_User_Service.ForgetPassword(model);
                if (result.Success)
                {
                    TempData["Message"] = result.Data;
                    return RedirectToAction("Index");
                }
                TempData["Message"] = result.ErrorMessages;
                return RedirectToAction("ForgetPassword");
            }
            return View();
        }
        //------------------Log out------------------------
        public ActionResult Logout()
        {
            Session["token"] = null;
            Session.Clear();
            return RedirectToRoute("Home");
        }
    }
}