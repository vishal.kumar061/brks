﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Controllers
{
    [CustomAuthorizationFilter]
    public class AttendenceController : Controller
    {
        private readonly IMA_User_Service MA_User_Service;
        private readonly IMA_Leave_Service MA_Leave_Service;

        public AttendenceController()
        {
            MA_User_Service = new MA_User_Service();
            MA_Leave_Service = new MA_Leave_Service();
        }

        // GET: Attendence
        public ActionResult Index(string month_year)
        {
            var model = MA_User_Service.GetAttendence(month_year);
            return View(model.Data);
        }
        [HttpPost]
        public ActionResult Index(FormCollection fc)
        {
            var model = MA_User_Service.SaveAttendence(fc);
            //var model = MA_User_Service.GetAttendence(month_year);
            return RedirectToAction("Index", new { month_year = fc["month_year"].ToString() });
        }
        //----------Leave_request------
        public ActionResult LeaveRequest()
        {
            var user = MA_User_Service.Get().ToList();
            var model = (from x in user.SelectMany(x => x.UserAttendences.ToList().Where(y => y.LeaveId != null && y.IsApproved == null).ToList())
                         select new LeaveRequestModel
                         {
                             Id = x.MA_User.Id,
                             UserAttendenceId = x.Id,
                             Name = x.MA_User.Name,
                             LeaveRequestedOn = Convert.ToDateTime(x.Date).ToString("dd-MMMM-yyyy"),
                             Reason = x.MA_Leave.Name
                         }).ToList();
            return View(model);
        }
        public ActionResult Approve_Reject_Attendence(int UserAttendenceId, bool isApproved)
        {
            var result = MA_User_Service.Approve_Reject_Attendence(UserAttendenceId, isApproved);
            return RedirectToAction("LeaveRequest");
        }

        //--------------Attendence--------------------

        public ActionResult AttendenceList(string Request_Date = "")
        {
            var Req_Date = string.IsNullOrEmpty(Request_Date) ? DateTime.Now.Date : Convert.ToDateTime(Request_Date).Date;
            var user = MA_User_Service.Get().ToList();
            var lst = (from x in user
                       select new AllUserAttendenceModel_Base
                       {
                           Id = x.Id,
                           Name = x.Name,
                           IsUserPresent = x.UserAttendences.ToList().Where(y => Convert.ToDateTime(y.Date).Date.Equals(Req_Date) && y.IsPresent == true).Count() > 0 ? true : false,
                           UserLeaveDetail = MA_User_Service.GetUserLeaveDetails(x.Id)
                       }).ToList();
            var model = new AllUserAttendenceModel
            {
                AllUserAttendenceModel_List = lst,
                Request_Date = Req_Date.ToString("dd-MMMM-yyyy"),
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult AttendenceList(AllUserAttendenceModel model)
        {
            return RedirectToAction("AttendenceList", new { model.Request_Date });
        }




        //-----AJAX
        [HttpGet]
        public ActionResult GetAllUserAttendence()
        {
            var userpresent = 0;
            var leaverequest = 0;
            var user = MA_User_Service.Get().ToList();
            foreach (var x in user)
            {
                var IsPresent = x.UserAttendences.ToList().Where(y => Convert.ToDateTime(y.Date).Date.Equals(DateTime.Now.Date) && y.IsPresent == true);
                userpresent = IsPresent.Count() > 0 ? userpresent += 1 : userpresent;

                var hasleavereq = x.UserAttendences.ToList().Any(y => y.LeaveId != null && y.IsApproved == null);
                leaverequest = hasleavereq ? leaverequest + 1 : leaverequest;
            }

            var obj = new
            {
                TotalUser = user.Count,
                Present = userpresent,
                Absent = user.Count - userpresent,
                Leaverequest = leaverequest,
                Users = (from x in user
                         select new
                         {
                             Id = x.Id,
                             Name = x.Name,
                         }).OrderBy(z => z.Name).ToList(),

                MA_Leave_Lst = (from x in MA_Leave_Service.GetAll()
                                select new
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    FromDate = x.FromDate,
                                    ToDate = x.FromDate,
                                    IsPaid = x.IsPaid
                                }).ToList()
            };
            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetAttendence(int UserId)
        {
            if (UserId != 0)
            {
                var UserAttendences = (from x in MA_User_Service.Get(UserId).UserAttendences
                                       select new
                                       {
                                           UserId = x.UserId,
                                           IsPresent = x.IsPresent,
                                           Date = x.Date,
                                           Location = x.Location,
                                           Remarks = x.Remarks == null ? "" : x.Remarks,
                                           Leave_Name = x.LeaveId == null ? "" : x.MA_Leave.Name,
                                           IsApproved = x.IsApproved,
                                       }).ToList();

                var obj = new
                {
                    UserAttendences = UserAttendences,
                    MA_Leave_Lst = (from y in MA_Leave_Service.GetAll()
                                    select new
                                    {
                                        Id = y.Id,
                                        Name = y.Name,
                                        FromDate = y.FromDate,
                                        ToDate = y.FromDate,
                                        IsPaid = y.IsPaid
                                    }).ToList()
                };

                return this.Json(obj, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        [HttpPost]
        public ActionResult SetAttendence(UserAttendence model)
        {
            var IsAdmin = Request.QueryString["IsAdmin"] == "1" ? true : false;
            var result = MA_User_Service.AddEdit_UserAttendence(model, IsAdmin);
            return Json(result);
        }



        //--------------UPLOAD EXCEL
        public FileResult Download_Format()
        {
            var path = Server.MapPath("~//Utility//ATTENDENCE_FORMAT.xlsx");
            return File(path, "application/vnd.ms-excel", "ATTENDENCE_FORMAT.xlsx");
        }

        public ActionResult Upload()
        {
            return View(new User_Attendence_Excel_Model());
        }
        [HttpPost]
        public ActionResult Upload(User_Attendence_Excel_Model model)
        {
            if (ModelState.IsValid)
            {
                var data = MA_User_Service.Upload(model);
                return View(data);
            }
            return View(model);
        }

        public ActionResult Save_Csv_Attendence()
        {
            MA_User_Service.Save_Csv_Attendence();
            return RedirectToAction("Index");
        }

    }


}