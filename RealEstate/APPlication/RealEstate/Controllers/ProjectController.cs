﻿using RealEstate.Config;
using RealEstate.Database.Service;
using System.Web.Mvc;
using RealEstate.Models;
namespace RealEstate.Controllers
{
    [CustomAuthorizationFilter]
    public class ProjectController : Controller
    {
        private readonly IMA_Project_Service MA_Project_Service;
        public ProjectController()
        {
            MA_Project_Service = new MA_Project_Service();
        }
        public ActionResult Index()
        {
            return View(MA_Project_Service.GetAll());
        }

        public ActionResult AddEditProject(int id = 0)
        {
            var model = new ProjectModel();
            if (id != 0)
            {
                var entity = MA_Project_Service.Get(id);
                model = entity.ToDataTable().ToModelOf<ProjectModel>();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditProject(ProjectModel model)
        {
            if (ModelState.IsValid)
            {
                var result = MA_Project_Service.AddEditProject(model);
                if (result.Success)
                    return RedirectToAction("Index");
                else
                    return View(model);
            }
            return View(model);

        }
        public ActionResult DeleteProject(int id)
        {
            var result = MA_Project_Service.DeleteProject(id);
            return RedirectToAction("Index");

        }


    }
}
